SET(CMAKE_CXX_COMPILER "/opt/intel/composerxe/linux/mpi/intel64/bin/mpiicpc")
SET(CMAKE_C_COMPILER "/opt/intel/composerxe/linux/mpi/intel64/bin/mpiicc")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/bin)

cmake_minimum_required(VERSION 2.8.12)
project(mumps)

include_directories(${CMAKE_SOURCE_DIR}/../../src/)
add_subdirectory(${CMAKE_SOURCE_DIR}/../../ build_util)
message("/home/maks/Documents/IJS/e62numcodes/src/third_party/MUMPS_5.1.1/include")
include_directories("/home/maks/Documents/IJS/e62numcodes/src/third_party/MUMPS_5.1.1/include")
link_directories("/home/maks/Documents/IJS/e62numcodes/src/third_party/MUMPS_5.1.1/lib" "${LIBSRC}/MUMPS_5.1.1/PORD/lib")

SET(MKLROOT /opt/intel/mkl)
link_directories("${MKLROOT}/lib/intel64")
include_directories("${MKLROOT}/include")



SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -Wall -fPIC -O3")
SET(LAPACK mkl_intel_lp64 mkl_intel_thread mkl_core m "-qopenmp")
SET(SCALAP mkl_scalapack_lp64 mkl_blacs_intelmpi_lp64)
SET(MUMPS dmumps mumps_common pord ifcore)

add_library(matrices matrices.cpp)

add_executable(run_mumps run_mumps.cpp)
target_link_libraries(run_mumps ${MUMPS} ${SCALAP} ${LAPACK} matrices)

add_executable(c_example c_example.c)
target_link_libraries(c_example ${MUMPS} ${SCALAP} ${LAPACK})
