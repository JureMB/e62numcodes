cd ../bin 

for f in ../params/generated/*.xml
do
	fn=${f::-4} #trim xml from filename
	./lid_driven_explicit_2D $fn
done

cd ../exec