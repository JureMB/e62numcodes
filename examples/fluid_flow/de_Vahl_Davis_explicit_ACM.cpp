/*
[Solution of de Vahl Davis natural convection benchmark test
 :Explicit artificial compressibility with CBS stabilization
 execute with parameters from params/, i.e. .lid_driven_explicit_2D ../params/lid_driven_re100
]
*/
#include "common.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "includes.hpp"
#include "mls.hpp"
#include "mlsm_operators.hpp"
#include "overseer.hpp"
#include "types.hpp"
#include <omp.h>

#define _MAX_ITER_ 500

using namespace mm;

typedef Vec2d vec_t;
typedef double scal_t;

Overseer<scal_t, vec_t> O;

int main(int arg_num, char* arg[]) {
    O.timings.addCheckPoint("time_start");
    O(arg_num, arg);

    O.timings.addCheckPoint("domain");
    RectangleDomain<vec_t> domain(O.domain_lo, O.domain_hi);  // domain definition

    //domain.fillUniformWithStep(O.d_space, O.d_space);
    domain.fillUniform(O.d_num - Vec<int, 2>(2), O.d_num );

    Range<int> all = domain.types.filter([](vec_t p) { return true; });
    Range<int> interior = domain.types > 0;
    Range<int> boundary = domain.types < 0;

    double tol = 1e-10;
    Range<int> top = domain.positions.filter([=](const Vec2d&p) { return std::abs(p[1] - O.domain_hi[1]) < tol; }),
            bottom = domain.positions.filter([=](const Vec2d&p) { return std::abs(p[1] - O.domain_lo[1]) < tol; }),
            right = domain.positions.filter([=](const Vec2d&p) { return std::abs(p[0] - O.domain_hi[0]) < tol && std::abs(p[1] - O.domain_hi[1]) > tol && std::abs(p[1] - O.domain_lo[1]) > tol; }),
            left = domain.positions.filter([=](const Vec2d&p) { return std::abs(p[0] - O.domain_lo[0]) < tol && std::abs(p[1] - O.domain_hi[1]) > tol && std::abs(p[1] - O.domain_lo[1]) > tol; });

    Range<int> mid_plane = domain.positions.filter([](vec_t p) { return p[1] == 0.5; });
    assert_msg(mid_plane.size() > 0, "midplane empty");

    Range<int> mid_plane_y = domain.positions.filter([](vec_t p) { return p[1] == 0.5; });
    Range<int> mid_plane_x = domain.positions.filter([](vec_t p) { return p[0] == 0.5; });

    domain.findSupport(O.n, all);

    prn(mid_plane_x.size());

    O.timings.addCheckPoint("setup");

    EngineMLS<vec_t, Monomials, NNGaussians> mls
            ({O.sigmaB, O.m},
             O.sigmaW);
    auto op = make_mlsm(domain, mls, all, false);

    O.timings.addCheckPoint("time loop");
    // init state
    Range<double> P1(domain.size(), 0);
    Range<double> P2(domain.size(), 0);

    Range<vec_t> v1(domain.size(), 0);
    Range<vec_t> v2(domain.size(), 0);

    Range<double> T1(domain.size(), O.T_init);
    Range<double> T2(domain.size(), O.T_init);

    // boundary conditions
    v1 = v2;
    T2[left] = O.T_hot;
    T2[right] = O.T_cold;
    T1 = T2;

    // hydrostatic initial pressure
    for (auto i : all) {
        P1[i] = O.g_0 * O.rho * domain.positions[i][1];
    }
    P2 = P1;

    std::chrono::high_resolution_clock::time_point time_1, time_2;
    int step_history = 0;

    for (int step = 0; step <= O.t_steps; ++step) {
        double C; //speed of sound
        Range<scal_t> div_v(domain.size(), 0);     // divergence
        time_1 = std::chrono::high_resolution_clock::now();
        int i, i_count, i_count_cumulative;

        for (i_count = 1; i_count < _MAX_ITER_; ++i_count){
            // Navier-Stokes
            #pragma omp parallel for private(i) schedule(static)
            for (i = 0; i < interior.size(); ++i) {
                int c = interior[i];
                v2[c] = v1[c] + O.dt * (-1 / O.rho * op.grad(P1, c)
                                        + O.mu / O.rho * op.lap(v1, c)
                                        - op.grad(v1, c) * v1[c]
                                        + O.g * (1 - O.beta * (T1[c] - O.T_ref)));
            }
            //Speed of sound
            Range<scal_t> norm = v2.map([](const vec_t& p) {return p.norm();});
            C = O.dl*std::max(*std::max_element(norm.begin(), norm.end()),O.v_ref);
            // Mass continuity
            #pragma omp parallel for private(i) schedule(static)
            for (i = 0; i < domain.size(); ++i) {
                div_v[i] = op.div(v2, i);
                P2[i] = P1[i] - C * C * O.dt * O.rho * div_v[i] +
                        O.dl2 * C * C * O.dt * O.dt * op.lap(P1, i);
            }
            P1.swap(P2);
            if(*std::max_element(div_v.begin(), div_v.end()) < O.max_div) break;
        }
        i_count_cumulative+= i_count;
        // heat transport
        #pragma omp parallel for private(i) schedule(static)
        for (i = 0; i < interior.size(); ++i) {
            int c = interior[i];
            T2[c] = T1[c] + O.dt * O.lam / O.rho / O.c_p * op.lap(T1, c) -
                    O.dt * v1[c].transpose() * op.grad(T1, c);
        }
        // heat Neumann condition
        #pragma omp parallel for private(i) schedule(static)
        for (i = 0; i < top.size(); ++i) {
            int c = top[i];
            T2[c] = op.neumann(T2, c, vec_t{0, -1}, 0.0);
        }
        #pragma omp parallel for private(i) schedule(static)
        for (i = 0; i < bottom.size(); ++i) {
            int c = bottom[i];
            T2[c] = op.neumann(T2, c, vec_t{0, 1}, 0.0);
        }

        // time step
        v1.swap(v2);
        T1.swap(T2);

        // intermediate IO
        if (step % (1 + O.t_steps / O.out_no) == 0 || step == O.t_steps - 1) {
            O.reOpenHDF();
            // assigning new time step
            O.hdf_out.createFolder("/step" + std::to_string(O.out_recordI));
            O.hdf_out.openFolder("/step" + std::to_string(O.out_recordI));
            O.hdf_out.setDoubleAttribute("TimeStep", step);
            // initial output :: nodes, supports and all other static stuff
            if (step == 0) {
                O.hdfWrite("support", domain.support);
                O.hdfWrite("types", domain.types);
                O.hdfWrite("pos", domain.positions);
            }

            //prepare Nusselt
            Range<scal_t> Nu; //normalized Nusselt
            for (auto c : all)
                Nu.push_back((v2[c][0]*T2[c]*O.c_p*O.rho/O.lam - op.grad(T2, c)[0])*
                                     O.height/(O.T_hot - O.T_cold));

            O.hdfWrite("Nu", Nu);
            O.hdfWrite("v", v2);
            O.hdfWrite("p", P2);
            O.hdfWrite("T", T2);
            O.hdfWrite("time", (O.dt * step));
            O.closeHDF();
            O.out_recordI++;
        }
        // on screen reports
        if ((O.on_screen > 0 && std::chrono::duration<double>(time_1 - time_2).count() > O.on_screen )
            || step == O.t_steps - 1 || step == 0) {
            double time_elapsed = (O.timings.getTimeToNow("time loop"));
            double time_estimated = time_elapsed * (O.t_steps / (step + 0.001) - 1);

            Range<scal_t> v_mid_x;  // mid plane velocity temp
            Range<scal_t> v_mid_y;  // mid plane velocity temp
            Range<scal_t> Nu_x;     // Nu

            for (auto c : mid_plane_x)
                v_mid_x.push_back(v2[c][0] * O.rho * O.c_p / O.lam);  // dimensional velocity
            for (auto c : mid_plane_y)
                v_mid_y.push_back(v2[c][1] * O.rho * O.c_p / O.lam);  // dimensional velocity

            for (auto c : left)
                Nu_x.push_back(-op.grad(T2, c)[0] * O.height /
                               (O.T_hot - O.T_cold));  // Nusselt

            std::cout.setf(std::ios::fixed, std::ios::floatfield);
            std::cout << "#:" << step << "-" << std::setprecision(1)
                      << (double) step / O.t_steps * 100 << " % " << std::setprecision(0)
                      << "exe[m]:" << time_elapsed / 60 << std::setprecision(0) << "-"
                      << time_estimated / 60 << " " << std::setprecision(4)
                      << "max-mid v_x:" << *std::max_element(v_mid_x.begin(), v_mid_x.end())
                      << std::setprecision(2)
                      << " C:" << C << " "
                      << " PV:" << static_cast<double> (i_count_cumulative) / (step - step_history + 1) << " "
                      << " div:" << *std::max_element(div_v.begin(), div_v.end()) << " "
                      << " Nu_l:" << std::accumulate(Nu_x.begin(), Nu_x.end(), 0.0) / Nu_x.size() << std::endl;
            i_count_cumulative = 0;
            time_2 = time_1;
            step_history = step;

            if ( std::isnan(*std::max_element(v_mid_x.begin(), v_mid_x.end()))) break;
        }
    }

    O.timings.addCheckPoint("time_end");
    double exec_time =
            std::chrono::duration<double>(O.timings.getTime("time_start", "time_end")).count();

    // DEBUG OUTPUTS
    Range<vec_t> pos_mid_x, pos_mid_y, pos_left;
    pos_mid_x = domain.positions[mid_plane_x];
    pos_mid_y = domain.positions[mid_plane_y];
    pos_left = domain.positions[left];

    O.debug_out << "exec_time = " << exec_time << ";\n";
    /*O.debug_out << "v_mid_x = " << v_mid_x << ";\n";
    O.debug_out << "v_mid_y = " << v_mid_y << ";\n";
    O.debug_out << "pos_mid_x = " << pos_mid_x << ";\n";
    O.debug_out << "pos_mid_y = " << pos_mid_y << ";\n";
    O.debug_out << "pos_left = " << pos_left << ";\n";*/
    O.debug_out << "Ra = " << O.Ra << ";\n";
    O.debug_out << "rho = " << O.rho << ";\n";
    O.debug_out << "cp = " << O.c_p << ";\n";
    O.debug_out << "lam = " << O.lam << ";\n";
}
