

close all
clc;clear;

cmap = open('cmap.mat');

fontname = 'Times';
set(0,'defaultaxesfontname',fontname);
set(0,'defaulttextfontname',fontname);

fontsize = 13;
set(0,'defaultaxesfontsize',fontsize);
set(0,'defaulttextfontsize',fontsize);

working_dir='../bin/out/'; addpath(working_dir);
case_name='thermo_fluid_cylinder_TFC_wip_3';
case_name='de_Vahl_Davis_explicit_ACM_DVD_Ra1e8_100';

%% PLOT DEFINITIONS
plot_pos        = 1;
    plot_IT     = 1;
    scale       = 3.5;

contour_resolution = [50,50]; level_step = 0.05;
%REGISTERED VARIABLES -- variables to load from HDF5
regV={'v', 'p', 'T', 'Nu'};
atts = {'time'};

load_data;

cold = pos(1,:) == 0;
res = 100;level_steps = 100;


for ii = 1:no_steps - 1;
    load_data;
   
    %%collect some data
    conv(ii, 1) = time;
    conv(ii, 2) = max(-Nu);
    conv(ii, 3) = mean(-Nu(cold));
    conv(ii, 4) = max(-Nu(cold));
    conv(ii, 5) = min(-Nu(cold));
    
    disp(ii/(no_steps - 1));
end

clearex conv;
save('data/time_analysis')
