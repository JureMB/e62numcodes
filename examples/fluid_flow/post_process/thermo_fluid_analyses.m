    close all;clc; clear;
    fontname = 'Times';
    set(0,'defaultaxesfontname',fontname);
    set(0,'defaulttextfontname',fontname);
    
    fontsize = 20;
    set(0,'defaultaxesfontsize',fontsize);
    set(0,'defaulttextfontsize',fontsize);
    Resolution=200;
    MS = 4;
  
    working_dir='../bin/out/';
%     working_dir='Q:/e62numcodes/examples/fluid_flow/bin/out/';
      
    regV={'v', 'p', 'T', 'Nu'};
    atts ={'time'};
    %load ref
    CaseN=8;
    %load HDF

    case_name='thermo_fluid_cylinder_TFC_wip_1';

    load_data;
    
    %% check Nusselt
    if 0
       format long
       left = pos(1,:) == 0;
       right = pos(1,:) == 1;
       disp(mean(Nu(right)))
       disp(mean(Nu(left)))
       disp(mean(mean(Nu)))
       f = createFig('ps','m2 1x2 p1','scale',1);hold on;
       plot(pos(2,left), Nu(left),'+');
    end
    
    if 0 %temp contpur
        res = 100;
         [X Y, T]=interpolateToMeshGrid(pos(1,:), pos(2,:) ,T, res, res, ...
        'interpolation', 'cubic', 'out', 'mesh'); 
        f = createFig('ps','m2 1x2 p2','scale',1);hold on;
        
        %thermal only -- Fig 16 from Dzemirdiz. 
        img = flipud(imread('data/cylinder/Ra6_dzemerPr10.jpg'));      
        image('CData',img,'XData',[0 1],'YData',[0 1])
        
        lvl = [0.05,0.15,0.25,0.35,0.45,0.55,0.65,0.75,0.85];
        
        lvl = [0.95, 0.849, 0.749, 0.649, 0.550, 0.449, 0.349, 0.249, 0.150, 0.049 ];
        contour(X,Y,T,'LevelList',lvl...
        ,'fill','off',...
        'showtext','on','linestyle',':','color','r','linewidth',1.5);
        
        %%
    end
    
     if 0 %Nu comp
          f = createFig('ps','m2 1x2 p1','scale',1);hold on;
          D_data = [2.5183e-1,3.9062e-3;2.6753e-1,2.7344e-2;3.0932e-1,5.0781e-2 ;5.8601e-1,1.1328e-1;1.0632,1.7578e-1;1.7164,2.3828e-1;2.5591,3.0078e-1;6.9512,5.5078e-1;9.6061,6.7578e-1;1.1521e1,7.4609e-1;1.3369e1,8.0078e-1;1.9523e1,9.4141e-1;1.9949e1,9.6484e-1 ;1.9915e1,9.7266e-1 ;1.8775e1,9.9609e-1;1.9949e1,9.6484e-1];
          cold = pos(1,:) == 0;
          plot(pos(2,cold),-Nu(cold),'+')
          plot(D_data(:,2),D_data(:,1),'o')
         
     end
     
     if 1 %time 
         load('data/time_analysis');
         createFig('ps','m1 2x4 p1');hold on; 
%          plot(conv(:,1),conv(:,2));
         plot(conv(:,1),conv(:,3));
%          plot(conv(:,1),conv(:,4));
%          plot(conv(:,1),conv(:,5));
         
     end
    