
close all;clc;clear
fontname = 'Times';
set(0,'defaultaxesfontname',fontname);
set(0,'defaulttextfontname',fontname);


fontsize = 20;
set(0,'defaultaxesfontsize',fontsize);
set(0,'defaulttextfontsize',fontsize);
Resolution=200;
MS = 4;
cmap = open('cmap.mat');

%% read De Vahl Davis reference data
[a,b] = xlsread('data/de_vahl_davis/reference/ref_data.csv');
ref.Ra = a(:,1);
ref.u_max = a(:,2);
ref.v_max = a(:,3);
ref.Nu = a(:,4);
ref.Ref = b(2:end,5);
%%

%% load data from series analysis
load('data/series_analysis.mat');


%% fields plots
if 1
    res = 100;
    level_steps = 100;
    
    for j = 1:1:6
        pos = cell2mat(fields.pos(j));
        T = cell2mat(fields.T(j));
        v = cell2mat(fields.v(j));
        Ra_ = fields.Ra(j);
        [x_t, y_t, T_]=interpolateToMeshGrid(pos(1,:), pos(2,:) ,T ,res, res, 'interpolation', 'cubic', 'out', 'mesh'); 
        [x_v, y_v, u_]=interpolateToMeshGrid(pos(1,:), pos(2,:) ,v(1,:) ,res, res, 'interpolation', 'cubic', 'out', 'mesh'); 
        [x_v, y_v, v_]=interpolateToMeshGrid(pos(1,:), pos(2,:) ,v(2,:) ,res, res, 'interpolation', 'cubic', 'out', 'mesh'); 

        f1 = createFig('ps', 'm2 1x2 p1');hold on
        contour(x_t,y_t,T_, 'fill', 'on', 'levelstep', (max(T)-min(T))/level_steps);
        colormap(cmap.cmap1);
        title(['$\rm  Ra = ', num2str(Ra_,'%2.2e') ,'$'], 'interpreter', 'latex');
        h = colorbar;
         ylabel(h, '$T$', 'interpreter', 'latex');
%         exportfig(f1, ['figures/DVD_cont_T_Ra',num2str(log10(Ra_))...
%                   ,'.png'], 'format', 'png', 'color', 'rgb', 'Resolution', Resolution, 'Bounds', 'loose')

        f2 = createFig('ps', 'm2 1x2 p2');hold on
        contour(x_v,y_v,(u_.^2+v_.^2).^0.5 , 'fill', 'on','levelstep',max(max(v))/level_steps);
        colormap('jet');
        h = colorbar;
        title(['$\rm  Ra = ', num2str(Ra_,'%2.2e') ,'$'], 'interpreter', 'latex');
        ylabel(h, '$\left\| {\bf{v}} \right\|$', 'interpreter', 'latex');
%         exportfig(f2, ['figures/DVD_cont_v_convRa',num2str(log10(Ra_))...
%                   ,'.png'], 'format', 'png', 'color', 'rgb', 'Resolution', Resolution, 'Bounds', 'loose')
    end
end

%% convergence plot
if 0
    cases = unique(conv.Ra);
    j = 1:6;
    for Ra_ = cases(j)
      i = conv.Ra == Ra_;
      ii = find(ref.Ra == Ra_);
      %% Nusselt comparison
      if 1
          f1 = createFig('ps', 'm2 1x2 p1');hold on
          LG = {};
          plot(conv.N(i), conv.Nu_right_mean(i),'+--','linewidth',2.5);LG=[LG,{'$\rm MLSM$'}];
          for xx = ii'
              if ref.Nu(xx) == 0 continue; end
              plot([min(conv.N),max(conv.N)],[ref.Nu(xx),ref.Nu(xx)],'+:','linewidth',2.5);
              LG=[LG,{['$\rm ref. - ',cell2mat(ref.Ref(xx)),'$']}];
          end
          xlabel('$N$','interpreter','latex')
          ylabel('$\rm Nu^{avg}$','interpreter','latex')
          legend(LG,'interpreter','latex');
          title(['$\rm Ra = ', num2str(Ra_,'%2.2e') ,'$'], 'interpreter', 'latex');
          exportfig(f1, ['figures/DVD_Nu_convRa',num2str(log10(Ra_))...
              ,'.png'], 'format', 'png', 'color', 'rgb', 'Resolution', Resolution, 'Bounds', 'loose')
      end
      
      %% v_max comparison
      if 1
          LG={};
          f1 = createFig('ps', 'm2 1x2 p2');hold on
          plot(conv.N(i), conv.v_mid_y(i) ,'+--','linewidth',2.5);LG=[LG,{'$\rm MLSM$'}];
          for xx = ii'
              plot([min(conv.N),max(conv.N)],[ref.u_max(xx),ref.u_max(xx)],'+:','linewidth',2.5);
              LG=[LG,{['$\rm ref. - ',cell2mat(ref.Ref(xx)),'$']}];
          end
          xlabel('$N$','interpreter','latex')
          ylabel('${v^{max}_y}\left( {x,y = 0.5} \right)$','interpreter','latex')
          legend(LG,'interpreter','latex');
          title(['$\rm Ra = ', num2str(Ra_,'%2.2e') ,'$'], 'interpreter', 'latex');
          exportfig(f1, ['figures/DVD_vMax_convRa',num2str(log10(Ra_))...
              ,'.png'], 'format', 'png', 'color', 'rgb', 'Resolution', Resolution, 'Bounds', 'loose')
      end
    end
end
