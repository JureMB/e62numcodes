/*
[Solution of Lid 2D Driven cavity : Explicit artificial compressibility with CBS stabilization
 execute with parameters from params/, i.e. .lid_driven_explicit_2D ../params/lid_driven_re100
]
*/
#include "common.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "includes.hpp"
#include "mls.hpp"
#include "mlsm_operators.hpp"
#include "overseer.hpp"
#include "types.hpp"
#include <omp.h>

using namespace mm;

typedef Vec2d vec_t;
typedef double scal_t;

Overseer<scal_t, vec_t> O;

int main(int arg_num, char* arg[]) {
    O.timings.addCheckPoint("time_start");
    O(arg_num, arg);

    O.timings.addCheckPoint("domain");
    RectangleDomain<vec_t> domain(O.domain_lo, O.domain_hi);  // domain definition

    domain.fillUniformWithStep(O.d_space, O.d_space);

    domain.findSupport(O.n);

    Range<int> all = domain.types.filter([](vec_t p) { return true; });
    Range<int> interior = domain.types > 0;
    Range<int> boundary = domain.types < 0;

    double tol = 1e-10;
    Range<int> top = domain.positions.filter([=](const vec_t& p) {
        return std::abs(p[1] - O.domain_hi[1]) < tol; });
    Range<int> bottom = domain.positions.filter([=](const vec_t& p) {
        return std::abs(p[1] - O.domain_lo[1]) < tol; });
    Range<int> mid_plane = domain.positions.filter([=](const vec_t& p) {
        return std::abs(p[1] - (O.domain_hi[1] + O.domain_lo[1])*0.5) < tol; });
    assert_msg(!mid_plane.empty(), "Midplane is empty!");

    // init state
    Range<double> P1(domain.size(), 0);
    Range<double> P2(domain.size(), 0);

    Range<vec_t> v1(domain.size(), 0);
    Range<vec_t> v2(domain.size(), 0);

    // boundary conditions
    v2[top] = vec_t({1, 0});
    v1 = v2;

    O.timings.addCheckPoint("setup");

    EngineMLS<vec_t, Monomials, NNGaussians> mls(O.m, O.sigmaW);
    auto op = make_mlsm(domain, mls, all, false);

    O.timings.addCheckPoint("time loop");

    for (int step = 0; step <= O.t_steps; ++step) {
        int i;
        #pragma omp parallel for private(i) schedule(static)
        for (i = 0; i < interior.size(); ++i) {
            int c = interior[i];
            // Navier-Stokes
            v2[c] = v1[c] + O.dt * (-1 / O.rho * op.grad(P1, c) +
                    O.mu / O.rho * op.lap(v1, c) -
                    op.grad(v1, c) * v1[c]);
        }
        int c;
        #pragma omp parallel for private(c) schedule(static)
        for (c = 0; c < domain.size(); ++c) {
            // Mass continuity
            P2[c] = P1[c] - O.dl * O.dl * O.dt * O.rho * op.div(v2, c) +
                    O.dl2 * O.dl * O.dt * O.dt * op.lap(P1, c);
        }
        v1.swap(v2);
        P1.swap(P2);
        // intermediate IO
        if (step % (1 + O.t_steps / O.out_no) == 0 || step == O.t_steps - 1) {
            O.reOpenHDF();
            // assigning new time step
            O.hdf_out.createFolder(format("/step%d", O.out_recordI));
            O.hdf_out.openFolder(format("/step%d", O.out_recordI));
            O.hdf_out.setDoubleAttribute("TimeStep", step);
            // initial output :: nodes, supports and all other static stuff
            if (step == 0) {
                O.hdfWrite("support", domain.support);
                O.hdfWrite("types", domain.types);
                O.hdfWrite("pos", domain.positions);
            }

            O.hdfWrite("v", v2);
            O.hdfWrite("p", P2);
            O.hdfWrite("T", v2.map([] (const Vec2d& v) { return v.norm(); }));
            O.closeHDF();

            // on screen reports
            double time_elapsed = (O.timings.getTimeToNow("time loop"));
            double time_estimated = time_elapsed * (O.t_steps / (step + 0.001) - 1);

            Range<scal_t> v_i;
            for (auto c : mid_plane) v_i.push_back(v2[c][1]);
            std::cout.setf(std::ios::fixed, std::ios::floatfield);
            std::cout << "step no:" << step << "\t" << std::setprecision(1)
                      << 100. * step / O.t_steps << " % " << std::setprecision(0)
                      << "exec time [m]:" << time_elapsed / 60 << std::setprecision(0) << "-"
                      << time_estimated / 60 << " " << std::setprecision(4)
                      << "max. mid. v:" << *std::max_element(v_i.begin(), v_i.end()) << "\n";
            O.out_recordI++;
        }
    }

    O.timings.addCheckPoint("time_end");
    double exec_time = O.timings.getTime("time_start", "time_end");
    O.timings.showTimings();
    // DEBUG OUTPUTS
    Range<vec_t> v_mid;
    Range<vec_t> pos_mid;
    v_mid = v2[mid_plane];
    pos_mid = domain.positions[mid_plane];

    O.debug_out << "exec_time = " << exec_time << ";\n";
    O.debug_out << "v_mid = " << v_mid << ";\n";
    O.debug_out << "pos_mid = " << pos_mid << ";\n";
}
