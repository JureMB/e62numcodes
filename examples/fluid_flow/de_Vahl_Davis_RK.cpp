/*
[Solution of de Vahl Davis natural convection benchmark test
 :Explicit artificial compressibility with CBS stabilization
 execute with parameters from params/, i.e. .lid_driven_explicit_2D ../params/lid_driven_re100
]
*/
#include "common.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "includes.hpp"
#include "mls.hpp"
#include "mlsm_operators.hpp"
#include "overseer.hpp"
#include "types.hpp"
#include <omp.h>
#include "integrators.hpp"

using namespace mm;

typedef Vec2d vec_t;
typedef double scal_t;

Overseer<scal_t, vec_t> O;

int main(int arg_num, char* arg[]) {
    O.timings.addCheckPoint("time_start");
    O(arg_num, arg);

    O.timings.addCheckPoint("domain");
    RectangleDomain<vec_t> domain(O.domain_lo, O.domain_hi);  // domain definition

    domain.fillUniformWithStep(O.d_space, O.d_space);

    Range<int> top_left = domain.positions.filter([](vec_t p) { return p[0] == 0.0 && p[1] == 1.0; });
    Range<int> top_right =
            domain.positions.filter([](vec_t p) { return p[0] == 1.0 && p[1] == 1.0; });
    Range<int> bottom_left =
            domain.positions.filter([](vec_t p) { return p[0] == 0.0 && p[1] == 0.0; });
    Range<int> bottom_right =
            domain.positions.filter([](vec_t p) { return p[0] == 1.0 && p[1] == 0.0; });
    Range<int> corners;
    corners.insert(corners.end(), top_left.begin(), top_left.end());
    corners.insert(corners.end(), top_right.begin(), top_right.end());
    corners.insert(corners.end(), bottom_left.begin(), bottom_left.end());
    corners.insert(corners.end(), bottom_right.begin(), bottom_right.end());

    //domain.types[corners].remove();
    //domain.positions[corners].remove();

    Range<int> all = domain.types.filter([](vec_t p) { return true; });
    Range<int> interior = domain.types > 0;
    Range<int> boundary = domain.types < 0;

    Range<int> top = domain.positions.filter([](vec_t p) { return p[1] == 1.0; });
    Range<int> bottom = domain.positions.filter([](vec_t p) { return p[1] == 0.0; });
    Range<int> mid_plane_y = domain.positions.filter([](vec_t p) { return p[1] == 0.5; });
    Range<int> mid_plane_x = domain.positions.filter([](vec_t p) { return p[0] == 0.5; });
    Range<int> right = domain.positions.filter([](vec_t p) { return p[0] == 1.0; });
    Range<int> left = domain.positions.filter([](vec_t p) { return p[0] == 0.0; });


    domain.findSupport(O.n);
    prn(mid_plane_x.size());

    // init state
    Range<double> P1(domain.size(), 0);
    Range<double> P2(domain.size(), 0);

    Range<vec_t> v1(domain.size(), 0);
    Range<vec_t> v2(domain.size(), 0);

    Range<double> T1(domain.size(), 0);
    Range<double> T2(domain.size(), 0);

    // boundary conditions
    v2[top] = 0;
    v1 = v2;
    T2[left] = O.T_hot;
    T2[right] = O.T_cold;

    T1 = T2;

    // hydrostatic initial pressure
    for (auto i : all) {
        P1[i] = O.g_0 * O.rho * domain.positions[i][1];
    }
    P2 = P1;

    O.timings.addCheckPoint("setup");

    EngineMLS<vec_t, Monomials, NNGaussians> mls
            (O.m,
             O.sigmaW);
    auto op = make_mlsm(domain, mls, all, false);

    O.timings.addCheckPoint("time loop");

    Range<scal_t> v_mid_x;  // mid plane velocity temp
    Range<scal_t> v_mid_y;  // mid plane velocity temp
    Range<scal_t> Nu_x;     // Nu


    auto derivative_v = [&](double, const Eigen::VectorXd& v) {
        Range<vec_t> rv = reshape<2>(v);
        Range<vec_t> der(rv.size(), vec_t(0.0));
        int i;
        #pragma omp parallel for private(i) schedule(static)
        for (i = 0; i < interior.size(); ++i) {
            int c = interior[i];
            // Navier-Stokes
            der[c] = -1 / O.rho * op.grad(P1, c) + O.mu / O.rho * op.lap(rv, c) -
                     op.grad(rv, c) * rv[c] + O.g * (1 - O.beta * (T1[c] - O.T_ref));
        }
        return reshape(der);
    };

    auto derivative_T = [&](double, const VecXd& T) {
        VecXd der = Eigen::VectorXd::Zero(T.size());
        int i;
        #pragma omp parallel for private(i) schedule(static)
        for (i = 0; i < interior.size(); ++i) {
            int c = interior[i];
            // Navier-Stokes
            der[c] = O.lam / O.rho / O.c_p * op.lap(T, c) - v1[c].dot(op.grad(T, c));
        }
        return der;
    };

    auto integrator_v = integrators::Explicit::RK4().solve(
            derivative_v, 0.0, O.time, O.dt, reshape(v1));
    auto integrator_T = integrators::Explicit::RK4().solve(
            derivative_T, 0.0, O.time, O.dt, reshape(T1));

    auto v_stepper = integrator_v.begin();
    auto T_stepper = integrator_T.begin();

    for (int step = 0; step <= O.t_steps; ++step) {
        ++v_stepper;
        v2 = reshape<2>(v_stepper.value());
        ++T_stepper;
        T2 = reshape(T_stepper.value());
        int i;
        // heat Neumann condition
        #pragma omp parallel for private(i) schedule(static)
        for (i = 0; i < top.size(); ++i) {
            int c = top[i];
            T2[c] = op.neumann(T2, c, vec_t{0, 1}, 0.0);
        }
        #pragma omp parallel for private(i) schedule(static)
        for (i = 0; i < bottom.size(); ++i) {
            int c = bottom[i];
            T2[c] = op.neumann(T2, c, vec_t{0, -1}, 0.0);
        }

        T_stepper.value() = reshape(T2);

        // Mass continuity
        #pragma omp parallel for private(i) schedule(static)
        for (i = 0; i < domain.size(); ++i) {
            P2[i] = P1[i] - O.dl * O.dl * O.dt * O.rho * op.div(v2, i) +
                    O.dl2 * O.dl * O.dt * O.dt * op.lap(P1, i);
        }
        // time step
        v1.swap(v2);
        P1.swap(P2);
        T1.swap(T2);

        // intermediate IO
        if (step % (1 + O.t_steps / O.out_no) == 0 || step == O.t_steps - 1) {
            O.reOpenHDF();
            // assigning new time step
            O.hdf_out.createFolder("/step" + std::to_string(O.out_recordI));
            O.hdf_out.openFolder("/step" + std::to_string(O.out_recordI));
            O.hdf_out.setDoubleAttribute("TimeStep", step);
            // initial output :: nodes, supports and all other static stuff
            if (step == 0) {
                O.hdfWrite("support", domain.support);
                O.hdfWrite("types", domain.types);
                O.hdfWrite("pos", domain.positions);
            }

            O.hdfWrite("v", v2);
            O.hdfWrite("p", P2);
            O.hdfWrite("T", T2);
            O.hdfWrite("time", (O.dt * step));
            O.closeHDF();
            O.out_recordI++;
        }
        // on screen reports
        if (step % (1 + O.t_steps / static_cast<int>(O.on_screen)) == 0 || step == O.t_steps - 1) {
            double time_elapsed = (O.timings.getTimeToNow("time loop"));
            double time_estimated = time_elapsed * (O.t_steps / (step + 0.001) - 1);

            v_mid_x.clear();
            v_mid_y.clear();
            Nu_x.clear();

            for (auto c : mid_plane_x)
                v_mid_x.push_back(v2[c][0] * O.rho * O.c_p / O.lam);  // dimensional velocity
            for (auto c : mid_plane_y)
                v_mid_y.push_back(v2[c][1] * O.rho * O.c_p / O.lam);  // dimensional velocity

            for (auto c : left)
                Nu_x.push_back(op.grad(T2, c)[0] * O.height /
                               (O.T_hot - O.T_cold));  // dimensional velocity

            std::cout.setf(std::ios::fixed, std::ios::floatfield);
            std::cout << "step no:" << step << "\t" << std::setprecision(1)
                      << (double) step / O.t_steps * 100 << " % " << std::setprecision(0)
                      << "exec time [m]:" << time_elapsed / 60 << std::setprecision(0) << "-"
                      << time_estimated / 60 << " " << std::setprecision(4)
                      << " max. mid. v_x:" << *std::max_element(v_mid_x.begin(), v_mid_x.end())
                      << std::setprecision(4)
                      << " max. Nu left:" << *std::max_element(Nu_x.begin(), Nu_x.end()) << "\n";
        }
    }

    O.timings.addCheckPoint("time_end");
    double exec_time = O.timings.getTime("time_start", "time_end");
    // DEBUG OUTPUTS

    Range<vec_t> pos_mid_x, pos_mid_y, pos_left;
    pos_mid_x = domain.positions[mid_plane_x];
    pos_mid_y = domain.positions[mid_plane_y];
    pos_left = domain.positions[left];

    O.debug_out << "exec_time = " << exec_time << ";\n";
    O.debug_out << "v_mid_x = " << v_mid_x << ";\n";
    O.debug_out << "v_mid_y = " << v_mid_y << ";\n";
    O.debug_out << "pos_mid_x = " << pos_mid_x << ";\n";
    O.debug_out << "pos_mid_y = " << pos_mid_y << ";\n";
    O.debug_out << "pos_left = " << pos_left << ";\n";
    O.debug_out << "Nu_x = " << Nu_x << ";\n";
}
