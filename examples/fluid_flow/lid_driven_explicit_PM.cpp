/*
[Solution of Lid 2D Driven cavity : Implicit pressure correction solution of Navier Stokes
 execute with parameters from params/, i.e. .lid_driven_explicit_2D ../params/lid_driven_re100
]
*/
#include "common.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "includes.hpp"
#include "mls.hpp"
#include "mlsm_operators.hpp"
#include "overseer.hpp"
#include "types.hpp"
#include <Eigen/Sparse>
#include <omp.h>

using namespace mm;
using namespace Eigen;

typedef Vec2d vec_t;
typedef double scal_t;

Overseer<scal_t, vec_t> O;
int main(int arg_num, char* arg[]) {
    O.timings.addCheckPoint("time_start");
    O(arg_num, arg);

    O.timings.addCheckPoint("domain");
    RectangleDomain<vec_t> domain(O.domain_lo, O.domain_hi);  // domain definition

    domain.fillUniformWithStep(O.d_space, O.d_space);

    Range<int> all = domain.types.filter([](vec_t p) { return true; });
    Range<int> interior = domain.types > 0;
    Range<int> boundary = domain.types < 0;

    double tol = 1e-10;
    Range<int> top = domain.positions.filter([=](const Vec2d&p) { return std::abs(p[1] - O.domain_hi[1]) < tol; }),
            bottom = domain.positions.filter([=](const Vec2d&p) { return std::abs(p[1] - O.domain_lo[1]) < tol; }),
            right = domain.positions.filter([=](const Vec2d&p) { return std::abs(p[0] - O.domain_hi[0]) < tol && std::abs(p[1] - O.domain_hi[1]) > tol && std::abs(p[1] - O.domain_lo[1]) > tol; }),
            left = domain.positions.filter([=](const Vec2d&p) { return std::abs(p[0] - O.domain_lo[0]) < tol && std::abs(p[1] - O.domain_hi[1]) > tol && std::abs(p[1] - O.domain_lo[1]) > tol; });

    Range<int> mid_plane = domain.positions.filter([](vec_t p) { return p[1] == 0.5; });
    assert_msg(mid_plane.size() > 0, "midplane empty");
    prn(mid_plane.size());
    domain.findSupport(O.n);

    // init state
    int N = domain.size();
    VecXd p_c(N, 0), p(N, 0); //pressure correction, pressure
    Range<vec_t> v1(N, 0); // previous velocity vector
    Range<vec_t> v2(N, 0); // new velocity vector

    v2[top] = vec_t({1, 0}); // vel BCs
    v1 = v2;

    O.timings.addCheckPoint("setup");

    EngineMLS<vec_t, Monomials, NNGaussians> mls(O.m, O.sigmaW * domain.characteristicDistance());
    auto op = make_mlsm(domain, mls, all);

    VecXd rhs_scal(N + 1, 0); //Note N+1, +1 stands for regularization equation

    O.timings.addCheckPoint("time loop");

    typedef SparseMatrix<double> matrix_t;
    matrix_t M_scal(N + 1, N + 1);  // matrix for scal equations
    SparseLU<matrix_t> solver;  // system solve
    M_scal.reserve(Range<int>(N + 1, O.n));

    // pressure boundary conditions dp/dn = 0;
    for (int i : top) op.der1(M_scal, 0, 1, i, 1.0);
    for (int i : bottom) op.der1(M_scal, 0, 1, i, 1.0);
    for (int i : left) op.der1(M_scal, 0, 0, i, 1.0);
    for (int i : right) op.der1(M_scal, 0, 0, i, 1.0);
    //Poisson equation
    for (int i : interior) op.lap(M_scal, i, 1.0);

    // Integral P = 0, average P = 0, referred to also as regularization
    for (int i = 0; i < N; ++i) {
        M_scal.coeffRef(N, i) = 1;
        M_scal.coeffRef(i, N) = 1;
    }
    rhs_scal(N) = 0; // = 0 part of the regularization equation

    M_scal.makeCompressed();
    solver.compute(M_scal);

    for (int step = 0; step <= O.t_steps; ++step) {
        //explicit velocity computatio
        #pragma omp parallel for  schedule(static)
        for (int i = 0; i < interior.size(); ++i) {
            int c = interior[i];
            // Navier-Stokes
            v2[c] = v1[c] + O.dt * ( O.mu / O.rho * op.lap(v1, c) -
                                    op.grad(v1, c) * v1[c]);
        }
        //pressure correction Poisson equation
        #pragma omp parallel for  schedule(static)
        for (int i = 0; i < interior.size(); ++i) {
            int c = interior[i];
            rhs_scal(c) = O.rho / O.dt * op.div(v2, c);
        }

        VecXd solution = solver.solve(rhs_scal);
        double alpha = solution[N];
        p_c = solution.head(N);
        // apply velocity correction
        #pragma omp parallel for  schedule(static)
        for (int i = 0; i < interior.size(); ++i) {
            int c = interior[i];
            v2[c] -=  O.dt / O.rho * op.grad(p_c, c);
        }

        // time advance
        p = p_c;
        v1.swap(v2);

        // intermediate IO
        if (step % (1 + O.t_steps / O.out_no) == 0 || step == O.t_steps - 1) {
            //prn(alpha);
            O.reOpenHDF();
            // assigning new time step
            O.hdf_out.createFolder("/step" + std::to_string(O.out_recordI));
            O.hdf_out.openFolder("/step" + std::to_string(O.out_recordI));
            O.hdf_out.setDoubleAttribute("TimeStep", step);
            // initial output :: nodes, supports and all other static stuff
            if (step == 0) {
                O.hdfWrite("support", domain.support);
                O.hdfWrite("types", domain.types);
                O.hdfWrite("pos", domain.positions);
            }

            O.hdfWrite("v", v2);
            O.hdfWrite("p", reshape(p_c));
            O.closeHDF();
            // on screen reports
            double time_elapsed = (O.timings.getTimeToNow("time loop"));
            double time_estimated = time_elapsed * (O.t_steps / (step + 0.001) - 1);

            Range<scal_t> v_i;
            for (auto c : mid_plane) v_i.push_back(v2[c][1]);
            std::cout.setf(std::ios::fixed, std::ios::floatfield);
            std::cout << "step no:" << step << "\t" << std::setprecision(1)
                      << (double) step / O.t_steps * 100 << " % " << std::setprecision(0)
                      << "exec time [m]:" << time_elapsed / 60 << std::setprecision(0) << "-"
                      << time_estimated / 60 << " " << std::setprecision(4)
                      << "alpha: " << alpha << " " << std::setprecision(6)
                      << "max. mid. v:" << *std::max_element(v_i.begin(), v_i.end()) << "\n";
            O.out_recordI++;
        }
    }
    O.timings.addCheckPoint("time_end");
    double exec_time =
            std::chrono::duration<double>(O.timings.getTime("time_start", "time_end")).count();

    // DEBUG OUTPUTS
    Range<vec_t> v_mid;
    Range<vec_t> pos_mid;
    v_mid = v2[mid_plane];
    pos_mid = domain.positions[mid_plane];
    O.timings.showTimings();

    O.debug_out << "exec_time = " << exec_time << ";\n";
    O.debug_out << "v_mid = " << v_mid << ";\n";
    O.debug_out << "pos_mid = " << pos_mid << ";\n";
    O.debug_out << "right = " << right << ";\n";
    O.debug_out << "left = " << left << ";\n";
    O.debug_out << "top = " << top << ";\n";
    O.debug_out << "bottom = " << bottom << ";\n";
}
