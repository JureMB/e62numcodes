% generates series of XML files for convergence analysis
% The parameters that are not considered in the below code are
% defined in DVD_convergence_child.xml file

clc;fclose('all');
parent_file = 'TFC_wip.xml';

out_dir = 'generated/';
out_dir = 'Q:/thermo/';
exec_scripts_out = out_dir ;
clean_dir = 0;

path_to_params = ''; %relative path from executable to generated 
prep_execute_scripts = 1;
kocke = [1:26];



numProcesses = 1;
executable = './thermo_fluid_cylinder';
if clean_dir
    if (7==exist(out_dir,'dir')) rmdir(out_dir, 's');end
    mkdir(out_dir)
end

i = 1; 
for dl2 = [0] 
for dl = [1, 0.9, 0.8, 0.75]    
for dt = [0.025, 0.01, 0.005] 
for N = [0.01, 0.008, 0.0075, 0.007]%, 0.0055, 0.0035] 
    offspring = [out_dir,sprintf('a%06d.xml',i)];
    copyfile(parent_file, offspring) 

    docNode = xmlread(offspring);
    a = docNode.getElementsByTagName('params');
    b = a.item(0).getElementsByTagName('num');
    b.item(0).setAttribute('dl2', num2str(dl2))
    b.item(0).setAttribute('dl', num2str(dl))
    b.item(0).setAttribute('dt', num2str(dt))
    b.item(0).setAttribute('d_space', num2str(N))
    
    b = a.item(0).getElementsByTagName('io');
    b.item(0).setAttribute('on_screen_num', '30')
    
    xmlwrite(offspring, docNode);
    i = i + 1;
end
end
end
end


%prepare execution lists for ninestein cluster.
if prep_execute_scripts == 1
    fileList = dir([out_dir, '/*.xml']);
    execute_list = sprintf('%s\\executeList.sh', exec_scripts_out); fid_execute_list = fopen(execute_list, 'w+');
    run_script = sprintf('%s\\run.sh', exec_scripts_out); fid_run_script = fopen(run_script, 'w+');
    kocka_start_script = sprintf('%s\\start.sh', exec_scripts_out); fid_kocka_start_script = fopen(kocka_start_script, 'w+');
    kocka_shutdown_script = sprintf('%s\\shutdown.sh', exec_scripts_out); fid_kocka_shutdown_script = fopen(kocka_shutdown_script, 'w+');
    status_script = sprintf('%s\\status.sh', exec_scripts_out); fid_status_script = fopen(status_script, 'w+');
    kill_script = sprintf('%s\\killall.sh', exec_scripts_out); fid_kill_script = fopen(kill_script, 'w+');
    checkExecOutputs = sprintf('%s\\checkExecution.sh', exec_scripts_out); fid_chkExec_script = fopen(checkExecOutputs, 'w+');
    checkErrors = sprintf('%s\\checkError.sh', exec_scripts_out); fid_chkError_script = fopen(checkErrors, 'w+');
    
    kocke = cellfun(@(x) strcat('k', num2str(x)), num2cell(uint8(kocke)), 'UniformOutput', false);
    
    fprintf(fid_kocka_start_script, 'echo ... starting kockas... \n sudo kocka-start %s ', sprintf('%s ', kocke{:}));
    
    fprintf(fid_run_script, ['clear;\n echo Running tests\n', 'echo On ', num2str(length(kocke)), ...
        ' kockas\n', 'echo with ', num2str(numProcesses), ' process per kocka\n']);
    fprintf(fid_run_script, ['echo using: ', sprintf('%s ', kocke{:})]);
    
    fprintf(fid_run_script, ['\n mkdir execReports \n kocka-forcerun -i executeList.sh -o execReports/report.out -k "', sprintf('%s ', kocke{:}), '" -p ', num2str(numProcesses)]);
    fprintf(fid_kocka_shutdown_script, 'echo ... shuting down kockas... \n sudo kocka-shutdown %s ', sprintf('%s ', kocke{:}));
    fprintf(fid_status_script, 'clear\n echo Check computations \n sudo kocka-run -k "%s" "ps -eo pid,user,%%cpu,args --sort %%cpu | tail -n 2"', sprintf('%s ', kocke{:}));
    fprintf(fid_kill_script, 'clear\n echo Kill all computations \n sudo kocka-run -k "%s" "pkill -u gkosec"', sprintf('%s ', kocke{:}));
    fprintf(fid_chkExec_script, 'clear\n echo Execution report: \n ls -1v execReports/*.out | xargs tail -n 2');
    fprintf(fid_chkError_script, 'clear\n echo Error report: \n ls -1v execReports/*report* | xargs tail -n 2');
    
    cnt = 0;
    for i = 1:length(fileList)
        %%NINESTEIN RUN COMMANDS
        tmp = fileList(i).name;
        tmp = tmp(1:strfind(tmp,'.')-1);
        fprintf(fid_execute_list, sprintf('%s %s > execReports/%s.out\n', executable, [path_to_params,tmp], tmp));
    end
end
fclose('all');