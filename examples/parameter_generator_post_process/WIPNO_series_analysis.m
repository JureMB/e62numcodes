close all
clear;
input = '../bin/out/';
out = 'data/';

addpath(input)

files = dir(fullfile(input,'*.m'));
files = {files.name};

conv = [];
for fn = files;
   f = cell2mat(fn);eval(f(1:end-2));
   conv = [conv; length(v_mid), exec_time, max(v_mid(:,2))];
   disp(fn);
end

fig = createFig('monitor', 2, 'divider', 1, 'divider_y', 2, 'xlabel', 'N', 'ylabel', 'exec time [s]'); 
plot(conv(:,1).^2, conv(:,2), '-+');
set(gca, 'XScale', 'log')
set(gca, 'YScale', 'log')
grid on;hold on

fig = createFig('monitor', 2, 'divider', 1, 'divider_y', 2, 'pos', 2, 'xlabel', 'N', 'ylabel', 'max v'); 
plot(conv(:,1).^2, conv(:,3),'-+');hold on;grid on
set(gca, 'XScale', 'log')


clear fig;
save([out,'series_analysis.mat'])