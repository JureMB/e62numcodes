function mainPlot()
tic    
%%SETUP
  close all
    clc;clear;
    
    fontname = 'Times';
    set(0,'defaultaxesfontname',fontname);
    set(0,'defaulttextfontname',fontname);
    
    fontsize = 13;
    set(0,'defaultaxesfontsize',fontsize);
    set(0,'defaulttextfontsize',fontsize);
    
    working_dir='../bin/out/';  addpath(working_dir);
    case_name='lid_driven_wip';
 
    %REGISTERED VARIABLES -- variables to load from HDF5
    regV={'v'};
    scattvar='p';

    %% PLOT DEFINITIONS
    plot_pos        = 1;
        plot_IT     = 1;
        scale       = 1;
    
    contour_resolution = [50,50];
        
    execute_user_function = 0;
    %% HDF5 PART
    fn=[working_dir,case_name,'.h5'];
    info = hdf5info(fn);
    no_steps=size(info.GroupHierarchy.Groups,2);
    curr_step=1;
    %% INIT LOAD
    support = h5read(fn,'/step0/support');
    types = h5read(fn,'/step0/types');
    pos = h5read(fn,'/step0/pos');
    
    %load variables
    ii=no_steps-1;  %which step
   
    %actual load
    step=['/step',num2str(ii),'/'];
    for var=[regV,scattvar] eval([cell2mat(var),'=','h5read(''',fn,''',''',[step,cell2mat(var)],''');']);end
    
    %%
    %FIGURE CONFIG
    fig=createFig('monitor',2,'divider',1,'divider_y',2,'pos',2,'scale',0);hold on;    
    
    axis('equal');

    %%REGISTER PLOTS
    rP={};iip=0;%registered plots - system variable thaT have to be filled for each plot
    rS={};iis=0;%registered scattered plots - system variable thaT have to be filled for each plot
    rV={};iiv=0;%registered vector plots
    
    %nodes plotting
    if plot_pos && exist('pos') && ~exist('pp') 
        if 1 %contour plot - only one allowed
           rC =  {'pos(1,:)', 'pos(2,:)', 'p' }; 
           pc = scontour(eval(cell2mat(rC(1))), eval(cell2mat(rC(2))), eval(cell2mat(rC(3))), contour_resolution(1), contour_resolution(1) )';
        end
        
        if 1 %register ordinary plot
            iip=iip+1;
            rP=[rP;{'pos(1,types<0)','pos(2,types<0)'}];
            xp=plot(eval(cell2mat(rP(end,1))),eval(cell2mat(rP(end,2))),'k.', 'markersize', 5);   
            pp(iip)=xp; 
        end
        
        if 0 %register scatter plot
            iis=iis+1;
            if ~exist(scattvar,'var') eval([scattvar,'=ones(1, length(v));']);end
            rS=[rS;{'pos(1,types>0)','pos(2,types>0)',[scattvar,'(types>0)']}];
            xp=scatter(eval(cell2mat(rS(end,1))),eval(cell2mat(rS(end,2))),[8],eval(cell2mat(rS(end,3))),'marker','.');   
            ps(iis)=xp; 
        end   
        
        if 1 %register quiver plot 
            iiv=iiv+1;
            rV=[rV;{'pos(1,:)','pos(2,:)','v(1,:)','v(2,:)'}];
            pv(iiv)=quiver(eval(cell2mat(rV(end,1))),eval(cell2mat(rV(end,2))),eval(cell2mat(rV(end,3))),eval(cell2mat(rV(end,4))),scale,'k');   
        end  
    end
  
    evalin('base','clear')
    W = who; 
    putvar(W{:});
    
    if plot_IT
        set(fig,'KeyPressFcn',@keyDownListener);
    end
    toc
end

%%IT TOOLS functions
function keyDownListener(~, event)
    switch event.Key
    case 'leftarrow'
        evalin('base',...
        'curr_step=curr_step-1;if (curr_step < 0) curr_step=no_steps-1; end;');
    case 'rightarrow'
        evalin('base',...
        'curr_step=curr_step+1;if (curr_step == no_steps) curr_step=0; end');
    end
    
    %reload registered data
    step=['/step',num2str(evalin('base','curr_step')),'/'];
    fn=evalin('base','fn');
    for var=evalin('base','[regV,scattvar]') evalin('base',[cell2mat(var),'=','h5read(''',fn,''',''',[step,cell2mat(var)],''');']);end
    
   %update registered ordinary plots 
    if 0
        rP=evalin('base','rP'); 
        for i=1:size(rP,1)
          set(evalin('base',['pp(',num2str(i),')']), 'xdata',evalin('base',['eval(cell2mat(rP(',num2str(i),',1)))']));  
          set(evalin('base',['pp(',num2str(i),')']), 'ydata',evalin('base',['eval(cell2mat(rP(',num2str(i),',2)))'])); 
        end
    end
    
    %update registered scatter plots 
    rS=evalin('base','rS'); 
    for i=1:size(rS,1)
      set(evalin('base',['ps(',num2str(i),')']), 'xdata',evalin('base',['eval(cell2mat(rS(',num2str(i),',1)))']));  
      set(evalin('base',['ps(',num2str(i),')']), 'ydata',evalin('base',['eval(cell2mat(rS(',num2str(i),',2)))']));
      set(evalin('base',['ps(',num2str(i),')']), 'cdata',evalin('base',['eval(cell2mat(rS(',num2str(i),',3)))']));  
    end
    
    %update registered vector plots 
    rV=evalin('base','rV'); 
    for i=1:size(rV,1)
      set(evalin('base',['pv(',num2str(i),')']), 'udata',evalin('base',['eval(cell2mat(rV(',num2str(i),',3)))']));  
      set(evalin('base',['pv(',num2str(i),')']), 'vdata',evalin('base',['eval(cell2mat(rV(',num2str(i),',4)))']));  
    end
    %update contour plot
    if(evalin('base', 'exist(''pc'',''var'')'))
        [~, ~, zz] = ...
        evalin('base', 'interpolateToMeshGrid(pos(1, :), pos(2, :), p, contour_resolution(1), contour_resolution(2), ''interpolation'', ''cubic'', ''out'', ''mesh'')');  
        x = evalin('base','pc'); 
        x.ZData = zz;
    end
    
    disp([num2str(evalin('base','curr_step'))]);
    
    % call user function
    if evalin('base', 'execute_user_function') WIPNO_user_function; end;
end




