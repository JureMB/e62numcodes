clc;clear;fclose('all');
parent_file = 'default.xml';
out_dir = 'generated/';
executable = 'test';
prep_execute_scripts = 1;
kocke = [31:36];
numProcesses = 1;


if (7 == exist(out_dir, 'dir')) rmdir(out_dir, 's'); end
mkdir(out_dir)


%% lid Driven convergence params
i = 1;
for N = 10.^[2:0.1:5]
    
    offspring = [out_dir, sprintf('a%06d.xml', i)];
    copyfile(parent_file, offspring)
    
    docNode = xmlread(offspring);
    a = docNode.getElementsByTagName('params');
    b = a.item(0).getElementsByTagName('num');
    b.item(0).setAttribute('d_space', num2str(1/floor(N)^0.5))
    
    xmlwrite(offspring, docNode);
    i = i + 1;
end

if prep_execute_scripts == 1
    fileList = dir([out_dir, '/*.xml']);
    execute_list = sprintf('%s\\executeList.sh', out_dir); fid_execute_list = fopen(execute_list, 'w+');
    run_script = sprintf('%s\\run.sh', out_dir); fid_run_script = fopen(run_script, 'w+');
    kocka_start_script = sprintf('%s\\start.sh', out_dir); fid_kocka_start_script = fopen(kocka_start_script, 'w+');
    kocka_shutdown_script = sprintf('%s\\shutdown.sh', out_dir); fid_kocka_shutdown_script = fopen(kocka_shutdown_script, 'w+');
    status_script = sprintf('%s\\status.sh', out_dir); fid_status_script = fopen(status_script, 'w+');
    kill_script = sprintf('%s\\killall.sh', out_dir); fid_kill_script = fopen(kill_script, 'w+');
    checkExecOutputs = sprintf('%s\\checkExecution.sh', out_dir); fid_chkExec_script = fopen(checkExecOutputs, 'w+');
    checkErrors = sprintf('%s\\checkError.sh', out_dir); fid_chkError_script = fopen(checkErrors, 'w+');
    
    kocke = cellfun(@(x) strcat('k', num2str(x)), num2cell(uint8(kocke)), 'UniformOutput', false);
    
    fprintf(fid_kocka_start_script, 'echo ... starting kockas... \n sudo kocka-start %s ', sprintf('%s ', kocke{:}));
    
    fprintf(fid_run_script, ['clear;\n echo Running tests\n', 'echo On ', num2str(length(kocke)), ...
        ' kockas\n', 'echo with ', num2str(numProcesses), ' process per kocka\n']);
    fprintf(fid_run_script, ['echo using: ', sprintf('%s ', kocke{:})]);
    
    fprintf(fid_run_script, ['\n mkdir execReports \n kocka-forcerun -i executeList.sh -o execReports/report.out -k "', sprintf('%s ', kocke{:}), '" -p ', num2str(numProcesses)]);
    fprintf(fid_kocka_shutdown_script, 'echo ... shuting down kockas... \n sudo kocka-shutdown %s ', sprintf('%s ', kocke{:}));
    fprintf(fid_status_script, 'clear\n echo Check computations \n sudo kocka-run -k "%s" "ps -eo pid,user,%%cpu,args --sort %%cpu | tail -n 2"', sprintf('%s ', kocke{:}));
    fprintf(fid_kill_script, 'clear\n echo Kill all computations \n sudo kocka-run -k "%s" "pkill -u gkosec"', sprintf('%s ', kocke{:}));
    fprintf(fid_chkExec_script, 'clear\n echo Execution report: \n ls -1v execReports/*.out | xargs tail -n 2');
    fprintf(fid_chkError_script, 'clear\n echo Error report: \n ls -1v execReports/*report* | xargs tail -n 2');
    
    
    cnt = 0;
    for i = 1:length(fileList)
        %%NINESTEIN RUN COMMANDS
        tmp = fileList(i).name;
        tmp = tmp(1:strfind(tmp,'.')-1);
        fprintf(fid_execute_list, sprintf('%s %s > execReports/%s.out\n', executable, tmp, tmp));
    end
end
fclose('all');