#include "domain.hpp"
#include "domain_extended.hpp"
#include "includes.hpp"
#include "mls.hpp"
#include "mlsm_operators.hpp"
#include "io.hpp"
#include "types.hpp"
#include <Eigen/Dense>
#include <Eigen/Sparse>

using namespace mm;
using namespace Eigen;

int main(int argc, char* argv[]) {
    // Physical parameters
    double E = 72.1e9;
    double v = 0.33;
    double force = 50000;
    // Derived parameters
    double lam = E * v / (1 - 2 * v) / (1 + v);
    double mu = E / 2 / (1 + v);

    // Closed form solution
    std::function<Vec2d(Vec2d)> analytical = [=](const Vec2d& p) {
        double x = p[0], y = p[1], r2 = x*x + y*y, factor = -force / (4.0*M_PI*mu);
        double ux = 2*x*y/r2 + 2*mu/(lam + mu)*std::atan2(y, x);
        double uy = (y*y-x*x)/r2 - (lam + 2*mu)/(lam + mu)*std::log(r2);
        return Vec2d(factor*ux, factor*uy);
    };

    // Domain [-1, 1] x [-1, -0.1]
    Vec2d low(-1.0, -1.0), high(1.0, -0.1);
    double step = 2.0 / 50.0;
    RectangleDomain<Vec2d> domain(low, high);
    domain.fillUniformWithStep(step, step);
    int N = domain.size();

    Range<int> internal = domain.types > 0, boundary = domain.types < 0;
    int support_size = 9;
    domain.findSupport(support_size);

    // Approximation engine definition
    double sigmaW = 1.0;
    EngineMLS<Vec2d, Monomials, NNGaussians> mls(
        Monomials<Vec2d>({{0, 0}, {0, 1}, {0, 2}, {1, 0}, {1, 1}, {1, 2}, {2, 0}, {2, 1}, {2, 2}}),
        NNGaussians<Vec2d>(domain.characteristicDistance() * sigmaW));

    // Initialize operators on all nodes
    auto op = make_mlsm(domain, mls, internal);

    // [Field declarations]
    SparseMatrix<double, RowMajor> M(2 * N, 2 * N);
    M.reserve(Range<int>(2 * N, 2 * support_size));

    VectorXd rhs(2 * N);

    // Set equation on interior
    for (int i : internal) {
        op.graddiv(M, i, lam + mu);
        op.lapvec(M, i, mu);
        rhs(i) = 0;
        rhs(i + N) = 0;
    }
    // Set boundary conditions
    for (int i : boundary) {
        M.coeffRef(i, i) = 1;  // boundary conditions on the boundary
        M.coeffRef(i + N, i + N) = 1;  // boundary conditions on the boundary
        Vec2d bc = analytical(domain.positions[i]);
        rhs(i) = bc[0];
        rhs(i + N) = bc[1];
    }
    M.makeCompressed();

    // Solve the sparse system
    BiCGSTAB<SparseMatrix<double, RowMajor>, IncompleteLUT<double>> solver;
    solver.preconditioner().setDroptol(1e-5);
    solver.preconditioner().setFillfactor(20);
    solver.setTolerance(1e-15);
    solver.setMaxIterations(300);
    solver.compute(M);
    VectorXd sol = solver.solve(rhs);

    Range<double> error(N);
    double maxuv = 0;
    for (int i = 0; i < N; ++i) {
        Vec2d uv = analytical(domain.positions[i]);
        maxuv = std::max(maxuv, std::max(std::abs(uv[0]), std::abs(uv[1])));
        error[i] = std::max(std::abs(uv[0] - sol[i]), std::abs(uv[1] - sol[i + N]));
    }

    double L_inf_error = *std::max_element(error.begin(), error.end()) / maxuv;
    prn(L_inf_error);
    std::cout << "If you choose a smaller discretization step, the error decreases." << std::endl;

    return 0;
}
