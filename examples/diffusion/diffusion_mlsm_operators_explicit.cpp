/*
[Explicit solution of a diffusion equation with a use of MLSM operators
with a mixed boundary conditions -- check wiki for more details
http://www-e6.ijs.si/ParallelAndDistributedSystems/MeshlessMachine/wiki/index.php/Analysis_of_MLSM_performance
]

The equation we would like to solve is
\[
    \nabla^2 T  = \frac{\partial T}{\partial t}
\]
*/

#include "common.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "draw.hpp"
#include "includes.hpp"
#include "mls.hpp"
#include "mlsm_operators.hpp"
#include "types.hpp"

using namespace mm;

/**
 * @brief Closed form solution of diffusion equation
 * @param pos spatial coordinate
 * @param t time
 * @param a size of domain
 * @param D diffusion constant
 * @param N no. of expansion
 * @return value of temperature
 */
double diff_closedform(const Vec2d& pos, double t, double a, double D, size_t N) {
    double T = 0;
    double f = M_PI / a;
    for (size_t n = 1; n < N; n = n + 2) {
        for (size_t m = 1; m < N; m = m + 2) {
            T += 16.0 / f / f / (n * m) * std::sin(n * f * pos[0]) * std::sin(m * f * pos[1]) *
                 std::exp(-D * t * ((n * n + m * m) * f * f));
        }
    }
    return T;
}

int main() {
    Timer timer;
    timer.addCheckPoint("start");

    double dx = 1./100.;
    int n = 12;  // support size
    int m = 3;  // monomial basis of second order, i.e. 6 monomials
    double time = 0.1;  // time
    double dt = 1e-5;  // time step
    int t_steps = std::ceil(time / dt);
    double sigma = 1;  // naive normalization

    // prep domain
    RectangleDomain<Vec2d> domain({0, 0}, {1, 1});
    domain.fillUniformWithStep(dx, dx);
    Range<int> interior = domain.types > 0;
    domain.findSupport(n);

    // init state
    VecXd T1(domain.size(), 1);
    VecXd T2(domain.size(), 1);

    // dirichlet BCs
    T2[domain.types < 0] = 0;
    T1 = T2;

    timer.addCheckPoint("shapes");
    // prep operators
    EngineMLS<Vec2d, Monomials, NNGaussians> mls(m, sigma);
    auto op = make_mlsm<mlsm::lap>(domain, mls, interior, false);
    timer.addCheckPoint("solve");

    // time stepping
    int tt;
    for (tt = 0; tt < t_steps; ++tt) {
        // new temp.
        for (auto& c : interior) {
            T2[c] = T1[c] + dt * op.lap(T1, c);
        }
        // time advance
        T1.swap(T2);
    }
    timer.addCheckPoint("end");

    // compute error
    Range<double> E2(domain.size(), 0);
    for (auto& c : interior) {
        E2[c] = std::abs(T1[c] - diff_closedform(domain.positions[c], tt * dt, 1, 1, 50));
    }
    std::cout << "Max error at t = " << tt*dt << ": "
              << *std::max_element(E2.begin(), E2.end()) << "\n";

    timer.showTimings();

    return 0;
}
