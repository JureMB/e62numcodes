cmake_minimum_required(VERSION 2.8.12)

# Setting compilers (must be before first project command)
# this has to be set in the main CMakeLists file of the project

# SET(CMAKE_C_COMPILER /usr/bin/gcc)
# SET(CMAKE_CXX_COMPILER /usr/bin/g++)

# SET(CMAKE_C_COMPILER /usr/bin/clang)
# SET(CMAKE_CXX_COMPILER /usr/bin/clang++)

# SET(CMAKE_C_COMPILER /usr/local/opt/llvm/bin/clang)
# SET(CMAKE_CXX_COMPILER /usr/local/opt/llvm/bin/clang++)

project(utils)

# Enable mmic compile flag by doing cmake -Dmmic=1
option(MKL "MKL" OFF)
option(mmic "mmic" OFF)
if(mmic)
    message("WARNING: 'mmic' flag is ON.\n"
            "Everything will compile with support for Intel(r) Xeon Phi(tm)\n"
            "Coprocessor. All libraries will gain -mmic flag when compiling.\n"
            "The following functionalities are now disabled:\n"
            "\t- io.hpp -> HDF5IO\n"
            "\t- util.hpp -> Monitor\n"
            "\t- draw.hpp -> all\n")
endif()

# DEBUG
#  set(CMAKE_VERBOSE_MAKEFILE ON)

# project dirs
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/bin)
set(BINDIR ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})  # shortcut
set(LIBSRC ${CMAKE_CURRENT_SOURCE_DIR}/src/third_party)

# output compile command for IDE's
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

if(CMAKE_CXX_FLAGS)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")  # if user included this then only add necessary flags
else()
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Wfloat-conversion -pedantic -O3 -fmax-errors=5 -std=c++11") # add -fopenmp for parallelization
endif()

if(mmic)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -mmic")
endif()

if(MKL)
    if(NOT(MKLROOT))
        if(DEFINED ENV{MKLROOT})
            set(MKLROOT $ENV{MKLROOT})
        else()
            set(MKLROOT "/opt/intel/compilers_and_libraries_2017.2.174/linux/mkl")
        endif()
    endif()
    message("MKLROOT is set to: ${MKLROOT}")

    # If MKL support is enabled make sure to include correct folders
    set(LMKL mkl_intel_lp64 mkl_intel_thread mkl_core)
    include_directories(${MKLROOT}/include/)
    link_directories(${MKLROOT}/lib/intel64/)
endif()

# Include folder for ANN, Eigen, ...
include_directories(SYSTEM PUBLIC ${LIBSRC}) # include third party things as system
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/src/)

# enable tests only when it is a direct build
if(CMAKE_SOURCE_DIR STREQUAL CMAKE_CURRENT_SOURCE_DIR)
    add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/test/)
else()
    add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/test/ EXCLUDE_FROM_ALL)
endif()

# generate docs
add_custom_target(docs COMMAND doxygen docs/Doxyfile > /dev/null
                  WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
# deploy docs
add_custom_target(deploy COMMAND ./scripts/deploy.sh DEPENDS docs
                  WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
# backup wiki
add_custom_target(static_wiki COMMAND ./scripts/backup_wiki_static.sh
                  WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
