#ifndef SRC_DOMAIN_EXTENDED_HPP_
#define SRC_DOMAIN_EXTENDED_HPP_

#include "common.hpp"
#include "domain.hpp"
#include "kdtree.hpp"
#include "types.hpp"

namespace mm {

/**
 * @file
 * @brief File implementing extra functionality for domains.  This is in a separate file,
 * because of its dependencies, which are large and their inclusion may not always be
 * wanted by default.
 * @example domain_extended_test.cpp
 */

/**
 * @brief Finds and sets support of a domain. For each point at `support_size` points
 * closest to it are found and stored in support attribute. Points are stored in non
 * descending fashion according to the distances from the queried points, the first one
 * being the point itself.
 * The squares of distances from point `i` to its support are stored in `distances[i]`
 * array.
 * @param support_size Size of support for each node.
 */
template <class vec_t>
void Domain<vec_t>::findSupport(int support_size) {
    findSupport(std::numeric_limits<scalar_t>::infinity(), support_size);
}

/**
 * Find support for a subset of nodes.
 * @param support_size Size of support for each node. Must be smaller or equal to the
 * number of nodes.
 * @param for_which Indexes of nodes for which to find the support.
 **/
template <class vec_t>
void Domain<vec_t>::findSupport(int support_size, const Range<int>& for_which) {
    findSupport(std::numeric_limits<scalar_t>::infinity(), support_size,
                for_which);
}

/**
 * Find support for a subset of nodes, and also looking for support only among
 *`search_among`.
 * @param support_size Size of support for each node. Must be smaller or equal to the
 * number of nodes.
 * @param for_which Indexes of nodes for which to find the support.
 * @param search_among Indexes of nodes among which to search for support.
 * @param force_self If true each node's support will contain itself regardless,
 * even if it is not in `search_among`.
 *
 * Example: (find support for boundary nodes, but support is made only of internal nodes)
 * @code
 * d.findSupport(1, d.types < 0, d.types > 0);
 * @endcode
 **/
template <class vec_t>
void Domain<vec_t>::findSupport(int support_size, const Range<int>& for_which,
                                const Range<int>& search_among, bool force_self) {
    findSupport(std::numeric_limits<scalar_t>::infinity(), support_size, for_which,
                search_among, force_self);
}

/**
 * Same as without radius, but only points within the radius are included in support.
 * If radius is infinite, this is equal to the first version.
 */
template <class vec_t>
void Domain<vec_t>::findSupport(scalar_t radius_squared, int max_support_size) {
    Range<int> all_ind(positions.size());
    std::iota(all_ind.begin(), all_ind.end(), 0);
    findSupport(radius_squared, max_support_size, all_ind);
}

/// Same as without radius, but only points within the radius are included in support.
template <class vec_t>
void Domain<vec_t>::findSupport(scalar_t radius_squared, int max_support_size,
                                const Range<int>& for_which) {
    Range<int> all_ind(positions.size());
    std::iota(all_ind.begin(), all_ind.end(), 0);
    findSupport(KDTree<vec_t>(positions), radius_squared, max_support_size,
                for_which, all_ind);
}

/// Same as without radius, but only points within the radius are included in support.
template <class vec_t>
void Domain<vec_t>::findSupport(scalar_t radius_squared, int max_support_size,
                                const Range<int>& for_which, const Range<int>& search_among,
                                bool force_self) {
    for (int x : search_among) {
        assert_msg(0 <= x && x < positions.size(), "Index %d out of range [%d, %d) in "
                "search_among.", x, 0, positions.size());
    }
    findSupport(KDTree<vec_t>(positions[search_among]), radius_squared, max_support_size,
                for_which, search_among, force_self);
}

/**
 * Overload with user provided kdtree. The tree must contain points with indexes listed in
 * `search_among`. The rest of parameters are the same as usual.
 */
template <class vec_t>
void Domain<vec_t>::findSupport(const KDTree<vec_t>& tree, scalar_t radius_squared,
                                int max_support_size, const Range<int>& for_which,
                                const Range<int>& search_among, bool force_self) {
    assert_msg(!positions.empty(), "Cannot find support in an empty domain.");
    assert_msg(max_support_size > 0, "Support size must be greater than 0, got %d.",
               max_support_size);
    assert_msg(tree.size() == search_among.size(), "Size of indexes to search among must match "
               "the size of the tree, but got %d vs. %d.", search_among.size(), tree.size());
    assert_msg(max_support_size <= tree.size(),
               "Support size (%d) cannot exceed number of points that we are searching among (%d).",
               max_support_size, search_among.size());
    assert_msg(radius_squared > 0, "Support radius must be greater than 0, got %s.",
               radius_squared);
    assert_msg(!for_which.empty(), "Set of nodes for which to find the support is empty.");
    assert_msg(!search_among.empty(), "Set of nodes to search among is empty.");
    for (int x : for_which) {
        assert_msg(0 <= x && x < positions.size(), "Index %d out of range [%d, %d) in for_which.",
                   x, 0, positions.size());
    }
    for (int x : search_among) {
        assert_msg(0 <= x && x < positions.size(), "Index %d out of range [%d, %d) in "
                   "search_among.", x, 0, positions.size());
    }

    int n = positions.size();
    support.resize(n);
    distances.resize(n);
    // Only clear those supports that we will re-write
    support[for_which] = Range<int>();
    distances[for_which] = Range<scalar_t>();
    bool is_inf = std::isinf(radius_squared);
    for (int i : for_which) {
        auto res = (is_inf) ? tree.query(positions[i], max_support_size)
                            : tree.query(positions[i], radius_squared, max_support_size);
        support[i].reserve(res.first.size());
        distances[i].reserve(res.first.size());
        if (force_self && (search_among[res.first[0]] != i)) {
            // Add current point to its support
            support[i].push_back(i);
            distances[i].push_back(0);
            // Remove the furthest point from support
            res.first.pop_back();
            res.second.pop_back();
        }
        for (int j : res.first) support[i].push_back(search_among[j]);
        for (scalar_t j : res.second) distances[i].push_back(j);
    }
}

/**
 * Distributes given nodes more uniformly using a form of simulated annealing,
 * resulting in a more uniform node distribution. The functions first finds `num_neighbours`
 * closest neighbours for all nodes. Then it moves each point according to the "force" induced by
 * it by its neighbours.
 * @param nodes Indices of the node to relax (move).
 * @param num_iterations Number of iterations of annealing to perform.
 * @param initial_heat Initial heat, usually between 0 and 1 (but could he more), higher heat
 * meas more volatile movement. High initial heat may cause divergence and erratic behaviour.
 * @param num_neighbours Number of neighbours that affect my movement. Default value -1 means
 * @f$3^{\textrm{dim}} - 1@f$.
 * @todo Do not modify support!
 */
template <class vec_t>
void Domain<vec_t>::relax(Range<int> nodes, int num_iterations, scalar_t initial_heat,
                          int num_neighbours) {
    int N = positions.size();
    if (num_neighbours == -1) num_neighbours = std::pow(3, vec_t::dimension) - 1;
    assert_msg(N >= num_neighbours, "Cannot relax a domain with less that num_neighbours nodes, "
               "num_neighbours = %d, N = %d.", num_neighbours, N);
    assert_msg(num_neighbours >= 1, "Cannot relax a domain with less that num_neighbours nodes, "
            "num_neighbours = %d, N = %d.", num_neighbours, N);
    for (int i : nodes) {
        assert_msg(0 <= i && i < N, "Node index %d out of range [0, %d) in relax.", i, N);
    }

    for (int c = 0; c < num_iterations; ++c) {
        findSupport(num_neighbours+1);
        Range<int> to_remove = {};
        for (int k = 0; k < nodes.size(); ++k) {
            int i = nodes[k];
            vec_t dp = 0;
            for (int j = 1; j < support[i].size(); ++j) {
                dp += (positions[i] - positions[support[i][j]]) /
                        std::max(std::pow(distances[i][j], 3), 1e-150);
            }
            scalar_t norm = dp.norm();
            if (norm < EPS) {
                dp += 0.5*vec_t::Random();
            } else {
                dp /= norm;
            }
            scalar_t characteristic_dist = std::max(
                    0.5*(distances[i][1] + distances[i].back()), EPS);
            scalar_t cooling_factor = (static_cast<scalar_t>(num_iterations - c) / num_iterations);
            dp *= initial_heat * cooling_factor * characteristic_dist;

            // If it is moved out of the domain, then we have a hole in the boundary --
            // convert this node to boundary node.
            positions[i] += dp;
            if (!contains(positions[i])) {
                positions[i] -= dp;
                bool success; vec_t proj;
                std::tie(success, proj) = projectPointToBoundary(positions[i] - dp, dp);
                if (success) {  // new point was successfully added to the boundary
                    // @todo(jureslak): check that point is not too close to others on the boundary
                    // change its location
                    positions[i] = proj;
                    // change its type
                    Range<int> support_types = types[support[i]];
                    int min_type = *std::min_element(support_types.begin(), support_types.end());
                    types[i] = std::min(static_cast<int>(NODE_TYPE::DEFAULT_BOUNDARY), min_type);
                    // do not move this node anymore
                    to_remove.push_back(k);
                }
            }
        }
        nodes[to_remove].remove();
    }
}
/**
 * Relaxes all internal nodes.
 * @sa Domain::relax(nodes, unm_iterations, initial_heat, num_neighbours)
 */
template <class vec_t>
void Domain<vec_t>::relax(int num_iterations, scalar_t initial_heat, int num_neighbours) {
    return relax(types > 0, num_iterations, initial_heat, num_neighbours);
}

/**
 * Returns unit normal to an interval, eq. vector 1. Here only for compilation reasons.
 */
template <typename vec_t>
vec_t Domain<vec_t>::getUnitNormal(const Range<Vec<scalar_t, 1>>& /* p */) {
    return {1};
}

/**
 * Returns right hand side unit normal given two points on boundary.
 * @param p List of two points on the boundary.
 */
template <typename vec_t>
vec_t Domain<vec_t>::getUnitNormal(const Range<Vec<scalar_t, 2>>& p) {
    assert_msg(p.size() == 2, "Two points are needed to compute the normal in 2d, got %d.",
               p.size());
    vec_t a = p[0], b = p[1];
    vec_t boundary_direction = b - a;
    boundary_direction /= boundary_direction.norm();
    return boundary_direction.getPerpendicular();
}

/**
 * Returns unit normal to the surface given by three points.
 * @param p List of three points on the boundary.
 */
template <typename vec_t>
vec_t Domain<vec_t>::getUnitNormal(const Range<Vec<scalar_t, 3>>& p) {
    assert_msg(p.size() == 3, "Three points are needed to compute the normal in 3d, got %d.",
               p.size());
    vec_t a = p[0], b = p[1], c = p[2];
    vec_t t1 = b - a, t2 = b - c;
    vec_t normal = t1.cross(t2);
    return normal / normal.norm();
}

/**
 * Projects `point` to the boundary. First finds two closest points on the
 * boundary, constructs a unit normal, does an exponential search for the other side and then uses
 * bisection to find the precise location of the boundary. the returned point lies within
 * `precision` of the analytical boundary. Works in 2D and 3D.
 *
 * @param point Point close to the boundary hinting where to add the boundary point.
 * @param tree KDtree containing the boundary points. This can be user supplied and is useful if
 * more subsequent calls will be performed to avoid rebuilding the tree every time.
 * @param precision The distance of the found point from the boundary will be at most `precision`.
 * @return Whether the addition was successful and the coordinates of the point added, if it was.
 *
 * Example:
 * @snippet domain_extended_test.cpp Add to boundary
 */
template <typename vec_t>
std::pair<bool, vec_t> Domain<vec_t>::projectPointToBoundary(
        const vec_t& point, const KDTree<vec_t>& tree, scalar_t precision) const {
    assert_msg(tree.size() > 0, "Cannot refine boundary on a domain with no boundary nodes.");
    static_assert(vec_t::dimension == 1 || vec_t::dimension == 2 || vec_t::dimension == 3,
                  "Currently available only in 1, 2 or 3 dimensions.");
    if (vec_t::dimension == 1) {  // noop in 1d
        return {false, point};
    }
    Range<int> idxs;
    Range<scalar_t> distances;
    std::tie(idxs, distances) = tree.query(point, vec_t::dimension);
    assert_msg(idxs.size() == vec_t::dimension, "Cannot enough closest points on boundary, %d "
            "points are required, but only %d available.", vec_t::dimension, idxs.size());

    // Find unit normal
    vec_t unit_normal = getUnitNormal(tree.get(idxs));
    return projectPointToBoundary(point, unit_normal, precision);
}

/**
 * Returns the projection of `point` to the boundary.
 * @sa Domain::projectPointToBoundary(point, tree, precision)
 * @sa Domain::projectPointToBoundary(point, normal, precision)
 */
template <typename vec_t>
std::pair<bool, vec_t> Domain<vec_t>::projectPointToBoundary(const vec_t& point,
                                                             scalar_t precision) const {
    Range<vec_t> boundary = positions[types < 0];
    assert_msg(!boundary.empty(), "Cannot refine boundary on a domain with no boundary nodes.");
    static_assert(vec_t::dimension == 1 || vec_t::dimension == 2 || vec_t::dimension == 3,
                  "Currently available only in 1, 2 or 3 dimensions.");
    return projectPointToBoundary(point, KDTree<vec_t>(boundary), precision);
}

/**
 * Projects given point along a normal to the boundary with given precision.
 * @sa Domain::projectPointToBoundary(point, tree, precision)
 * @sa Domain::projectPointToBoundary(point, precision)
 */
template <typename vec_t>
std::pair<bool, vec_t> Domain<vec_t>::projectPointToBoundary(
        const vec_t& point, vec_t unit_normal, scalar_t precision) const {
    // Find point on the other side of the boundary with exponential search
    vec_t start = point;
    bool is_inside = contains(start);
    scalar_t max_stretch = characteristicDistance() / 100;
    while (true)  {
        if (contains(start + max_stretch * unit_normal) != is_inside) break;
        if (contains(start - max_stretch * unit_normal) != is_inside) { max_stretch *= -1; break; }
        max_stretch *= 2;
        if (std::isinf(max_stretch)) {  // hint is bad
            return {false, vec_t()};
        }
    }

    // Find the point on the boundary using bisection
    scalar_t stretch = max_stretch;
    while (std::abs(stretch) > precision) {
        stretch /= 2;
        if (contains(start + stretch * unit_normal) == is_inside) {
            start += stretch * unit_normal;
        }
    }

    // Make unit normal point outside
    if (is_inside) {
        unit_normal *= signum(max_stretch);
    } else {
        unit_normal *= -signum(max_stretch);
    }

    // Make sure point is inside (it's maybe EPS away)
    while (!contains(start)) start -= precision * unit_normal;

    return {true, start};
}

/**
 * @brief Refine a region of nodes `region` by connecting every node in `region` to its support
 * domain and generating new nodes at half distances. The new nodes are filtered to meet a
 * minimum distance criterion to prevent points that would be too close.
 *
 * @param region Node indexes around which to refine.
 * @param fraction Minimal distance criterion around point `p` is that nodes generated at `p` must
 * be at least `fraction * closest_support` away from all nodes. Fraction must be less than `1/2`.
 * @return The indexes of the added nodes in `positions`.
 *
 * Example: (refine all nodes)
 * @code
 * d.refine(d.types != 0, 0.4);
 * @endcode
 */
template <class vec_t>
Range<int> Domain<vec_t>::refine(Range<int> region, scalar_t fraction) {
    int region_size = region.size();
    int N = positions.size();

    assert_msg(region_size > 0, "The region to refine is empty.");

    // Refine relies on projectPointToBoundary that has "noop" function
    // in case of 1D domain. Realistically though, this refine function is
    // unnecessarily complex for 1D refinement.
    static_assert(vec_t::dimension < 4, "Only available in 1, 2 or 3 dimensions");

    assert_msg(support.size() == size(), "Support vector size does not match domain size "
            "(%d vs %d). Did you build support before calling refine?",
            support.size(), size());

    // sort: boundary nodes first
    std::sort(region.begin(), region.end(), [&](int i, int j) { return types[i] < types[j]; });
    KDTree<vec_t> domain_tree(positions);
    KDTree<vec_t> boundary_tree(positions[types < 0]);

    // Remember which nodes added which points.
    Range<Range<int>> children;

    // Iterate through points in region and generate new points
    double max_support_radius = 0;
    for (int i = 0; i < N; ++i) {
        max_support_radius = std::max(max_support_radius, distances[i].back());
    }
    max_support_radius = std::sqrt(max_support_radius);

    return refine_impl(region, fraction, domain_tree, boundary_tree, children, max_support_radius);
}

/**
 * Refine implementation with more control over fine grained details.
 */
template <class vec_t>
Range<int> Domain<vec_t>::refine_impl(
        Range<int> region, double fraction, const KDTree<vec_t>& domain_tree,
        const KDTree<vec_t>& boundary_tree, Range<Range<int>>& children,
        double max_support_radius) {
    int region_size = region.size();
    int N = positions.size();

    children.resize(children.size() + region_size);

    // sort: boundary nodes first
    std::sort(region.begin(), region.end(), [&](int i, int j) { return types[i] < types[j]; });
    KDTree<vec_t> region_tree(positions[region]);

    // Iterate through points in region and generate new points
    int num_new_points = 0;
    for (int i = 0; i < region_size; i++) {
        int c = region[i];  // the global domain index
        auto& pos = positions[c];
        auto& supp = support[c];
        double closest = std::sqrt(distances[c][1]);
        scalar_t min_dist = fraction * closest;

        // then find all the nodes in the radius that admit collisions of children
        double critical_radius = max_support_radius + min_dist;
        // Lahko kličemo tudi za vsak node posebej.
        Range<int> critical_idx = region_tree.query(pos, critical_radius*critical_radius,
                                                    region_size).first;

        // half links to my support
        for (int j = 1; j < supp.size(); j++) {
            int s = supp[j];
            vec_t candidate = 0.5*(positions[c] + positions[s]);

            // decide the type of new node and project to boundary if necessary
            int candidate_type = std::max(types[c], types[s]);
            if (candidate_type < 0) {
                auto result = projectPointToBoundary(candidate, boundary_tree, EPS);
                if (result.first) {
                    candidate = result.second;
                } else {
                    std::cerr << format("Adding point %s with type %d to the boundary was"
                                        "not successful.", candidate, candidate_type) << std::endl;
                    continue;
                }
            }

            // If nodes are out of the domain, they should be thrown away.
            if (!contains(candidate)) continue;

            // Check that it is not too close to old nodes
            double closest_old_node_dist_squared = domain_tree.query(candidate).second[0];
            bool too_close = closest_old_node_dist_squared < min_dist * min_dist;

            // Check possible collision with new nodes
            // Condition: dist(me, an old node's child) < min_dist
            // Because children are half-links with support, it can only happen if
            // d(p, q) <= max_support_size + min_dist
            if (!too_close) {
                for (int k : critical_idx) {  // check children of k
                    for (int l : children[k]) {
                        if ((positions[l] - candidate).squaredNorm() < min_dist * min_dist) {
                            too_close = true;
                            break;
                        }
                    }
                    if (too_close) break;
                }
            }

            if (!too_close) {  // add new point
                children[i].push_back(N + num_new_points++);  // increase new point counter
                addPoint(candidate, candidate_type);
            }
        }
    }

    // resize support and distances
    support.resize(N+num_new_points);
    distances.resize(N+num_new_points);

    // return back indices of new nodes
    Range<int> new_ids(num_new_points);
    std::iota(new_ids.begin(), new_ids.end(), N);

    return new_ids;
}

}  // namespace mm

#endif  // SRC_DOMAIN_EXTENDED_HPP_
