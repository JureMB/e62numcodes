#include "common.hpp"  // should not use any other user-defined libraries

#include "gtest/gtest.h"

namespace mm {

TEST(Common, SignumZero) {
    assert(signum(0.0f) == 0);
    assert(signum(0l) == 0);
    assert(signum(0ull) == 0);
    assert(signum(0.0l) == 0);
    assert(signum(0) == 0);
    assert(signum(0.0) == 0);
}

TEST(Common, SignumInfNan) {
    // just to define behaviour
    double nan = std::numeric_limits<double>::quiet_NaN();
    double inf = std::numeric_limits<double>::infinity();
    assert(signum(nan) == 0);
    assert(signum(inf) == 1);
    assert(signum(-inf) == -1);
}

TEST(Common, SignumPositive) {
    assert(signum('a') == 1);
    assert(signum(5.4) == 1);
    assert(signum(1e-7) == 1);
    assert(signum(23452345ull) == 1);
    assert(signum(1e20f) == 1);
    assert(signum(-23452345ull) == 1);
}

TEST(Common, SignumNegative) {
    assert(signum(-5.4) == -1);
    assert(signum(-1e-7) == -1);
    assert(signum(-1e20f) == -1);
    assert(signum(-23452345ll) == -1);
}

TEST(Common, FloorCeil) {
    EXPECT_EQ(1, ifloor(1.3));
    EXPECT_EQ(-2, ifloor(-1.3));
    EXPECT_EQ(2, iceil(1.3));
    EXPECT_EQ(-1, iceil(-1.3));
}

TEST(Random, GetSeed) {
    assert(get_seed() != get_seed());
}

TEST(Print, Format) {
    std::stringstream ss;
    std::vector<int> a = {1, 2, 3};
    print_formatted(a, "{", ", ", "}", ";", ss);
    EXPECT_EQ(std::string("{1, 2, 3};"), ss.str());

    std::stringstream sss;
    std::vector<std::vector<int>> b = {{1, 1}, {-1, 2}, {4, 3}};
    print_formatted(b, "{", ", ", "}", ";", sss);
    EXPECT_EQ(std::string("{{1, 1}, {-1, 2}, {4, 3}};"), sss.str());
}

TEST(Print, Mem2Str) {
    EXPECT_EQ("16 B", mem2str(16));
    EXPECT_EQ("1.6 kB", mem2str(1600));
    EXPECT_EQ("1.7 kB", mem2str(1682));
    EXPECT_EQ("1 MB", mem2str(1024*1024));
    EXPECT_EQ("5.9 GB", mem2str(5.5*1024*1024*1024));
    EXPECT_EQ("5.6 GB", mem2str(5.6*1000*1000*1000));
}

TEST(Text, Format) {
    EXPECT_EQ("test7.txt", format("test%d.txt", 7));
    EXPECT_EQ("test07.txt", format("test%02d.txt", 7));
    EXPECT_EQ("3.141596", format("%.6f", 3.14159625671837));
    EXPECT_EQ("a1b1c", format("%s%s%s%s%s", 'a', 1, "b", 1u, "c"));
}

TEST(Text, Split) {
    EXPECT_EQ(std::vector<std::string>({"a", "b", "c"}), split("a,b,c", ','));
    EXPECT_EQ(std::vector<std::string>({"a", "b", "c"}), split("a, b, c", ", "));
    EXPECT_EQ(std::vector<std::string>({"", "", "", " ", ""}), split("aaa a", "a"));
    EXPECT_EQ(std::vector<std::string>({"a", "b", ""}), split("a,b,", ","));
    EXPECT_EQ(std::vector<std::string>({"a", "b", ""}), split("a,,b,,", ",,"));
}

TEST(Text, Join) {
    EXPECT_EQ("ababa", join({"a", "a", "a"}, 'b'));
    EXPECT_EQ("ababa", join({"a", "a", "a"}, "b"));
    EXPECT_EQ("abxabxa", join({"a", "a", "a"}, "bx"));
    EXPECT_EQ("aa", join({"a", "", "a"}, ""));
    EXPECT_EQ("a123123123123a123", join({"a", "", "", "", "a", ""}, "123"));
}

TEST(Timer, getTime) {
    Timer t;
    t.addCheckPoint("a");
    t.addCheckPoint("b");
    double diff = t.getTime("a", "b");
    EXPECT_LT(diff, 1e-5);
    EXPECT_GT(diff, 0);
    double diff2 = t.getTimeToNow("a");
    EXPECT_LT(diff, diff2);
    EXPECT_GT(diff2, 0);
}

TEST(Timer, Stopwatch) {
    Timer t;
    // Same as Timer.getTime test.
    std::chrono::milliseconds sleep_time(10);
    t.startWatch("a");
    t.stopWatch("a");
    double diff = t.getCumulativeTime("a");
    EXPECT_GT(diff, 0);
    EXPECT_LT(diff, 1e-4);
    EXPECT_EQ(t.getNumLaps("a"), 1);
    // Testing loop.
    int n_loops = 10;
    std::chrono::high_resolution_clock::time_point t1 =
            std::chrono::high_resolution_clock::now();
    for (int i = 0; i < n_loops; i++) {
        t.startWatch("b");
        std::this_thread::sleep_for(sleep_time);
        t.stopWatch("b");
    }
    std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> time_span =
            std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
    double per_lap_ref = time_span.count() / n_loops;
    EXPECT_EQ(n_loops, t.getNumLaps("b"));
    double per_lap = t.getTimePerLap("b");
    EXPECT_LT(per_lap, per_lap_ref);
    EXPECT_LT(per_lap_ref - per_lap, 1e-4);
    // Testing loop - two stopwatches. The enclosing one is assumed
    // to be correct since the previous test did pass.
    for (int i = 0; i < n_loops; i++) {
        t.startWatch("d");
        t.startWatch("c");
        std::this_thread::sleep_for(sleep_time);
        t.stopWatch("c");
        t.startWatch("c");
        std::this_thread::sleep_for(sleep_time);
        t.stopWatch("c");
        t.stopWatch("d");
    }
    EXPECT_EQ(2*n_loops, t.getNumLaps("c"));
    double per_lap_ref_2 = t.getTimePerLap("d") / 2;
    double per_lap_2 = t.getTimePerLap("c");
    EXPECT_LT(per_lap_2, per_lap_ref_2);
    EXPECT_LT(per_lap_ref_2 - per_lap_2, 2*1e-4);
    t.stopwatchClear();
}

TEST(Bisection, Solve) {
    auto func = [](double x) -> double {
        return (3*std::sin(x*x + 0.4) + 0.1);};
    double target = 2.35;
    double a = bisection(func, 0.0, 1.0, target, 1e-6, 20);
    EXPECT_NEAR(target, func(a), 1e-5);
}

TEST(Bisection, HitMaxIterations) {
    auto func = [](double x) -> double {
        return (3*std::sin(x*x + 0.4) + 0.1);};
    double target = 2.35;
    double a = bisection(func, 0.0, 1.0, target, 1e-6, 5);  // must print error message
    EXPECT_NEAR(target, func(a), 1e-2);
}

}  // namespace mm
