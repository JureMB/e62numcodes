#include "util.hpp"
#include "common.hpp"

#include "gtest/gtest.h"

namespace mm {

TEST(Linspace, 1DBorder) {
    std::vector<double> expected = {1, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2};
    Range<Vec1d> result = linspace(Vec1d(1.0), {2}, {11});
    ASSERT_EQ(expected.size(), result.size());
    for (size_t i = 0; i < expected.size(); ++i) {
        EXPECT_DOUBLE_EQ(expected[i], result[i][0]);;
    }
}

TEST(Linspace, 1DBorderReversed) {
    std::vector<double> expected = {2, 1.9, 1.8, 1.7, 1.6, 1.5, 1.4, 1.3, 1.2, 1.1, 1};
    Range<Vec1d>result = linspace(Vec1d(2.0), {1}, {11});
    ASSERT_EQ(expected.size(), result.size());
    for (size_t i = 0; i < expected.size(); ++i) {
        EXPECT_DOUBLE_EQ(expected[i], result[i][0]);;
    }
}

TEST(Linspace, DoubleBorder) {
    std::vector<double> expected = {1, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2};
    Range<double> resultint = linspace(1, 2, 11);
    ASSERT_EQ(expected.size(), resultint.size());
    for (size_t i = 0; i < expected.size(); ++i) {
        EXPECT_DOUBLE_EQ(expected[i], resultint[i]);;
    }
}

TEST(Linspace, 1DNoBorder) {
    std::vector<double> expected = {1.9, 1.8, 1.7, 1.6, 1.5, 1.4, 1.3, 1.2, 1.1};
    Range<Vec1d> result = linspace(Vec1d(2.0), {1}, {9}, false);
    ASSERT_EQ(expected.size(), result.size());
    for (size_t i = 0; i < expected.size(); ++i) {
        EXPECT_DOUBLE_EQ(expected[i], result[i][0]);;
    }
}

TEST(Linspace, DoubleNoBroder) {
    std::vector<double> expected = {1.9, 1.8, 1.7, 1.6, 1.5, 1.4, 1.3, 1.2, 1.1};
    Range<double> resultint = linspace(2, 1, 9, false);
    ASSERT_EQ(expected.size(), resultint.size());
    for (size_t i = 0; i < expected.size(); ++i) {
        EXPECT_DOUBLE_EQ(expected[i], resultint[i]);;
    }
}

TEST(Linspace, 2DBorder) {
    std::vector<std::vector<double>> expected = {
        {  1, 3}, {  1, 3.1}, {  1, 3.2}, {  1, 3.3}, {  1, 3.4}, {  1, 3.5},
        {1.1, 3}, {1.1, 3.1}, {1.1, 3.2}, {1.1, 3.3}, {1.1, 3.4}, {1.1, 3.5},
        {1.2, 3}, {1.2, 3.1}, {1.2, 3.2}, {1.2, 3.3}, {1.2, 3.4}, {1.2, 3.5}};
    Range<Vec2d> result = linspace(Vec2d{1, 3}, {1.2, 3.5}, {3, 6});
    ASSERT_EQ(expected.size(), result.size());
    for (size_t i = 0; i < expected.size(); ++i) {
        EXPECT_DOUBLE_EQ(expected[i][0], result[i][0]);;
        EXPECT_DOUBLE_EQ(expected[i][1], result[i][1]);;
    }
}

TEST(Linspace, 2DNoBorder) {
    std::vector<std::vector<double>> expected = {
        {1.1, 3.1}, {1.1, 3.2}, {1.1, 3.3}, {1.1, 3.4}};
    Range<Vec2d> result = linspace(Vec2d{1, 3}, {1.2, 3.5}, {1, 4}, false);
    ASSERT_EQ(expected.size(), result.size());
    for (size_t i = 0; i < expected.size(); ++i) {
        EXPECT_DOUBLE_EQ(expected[i][0], result[i][0]);;
        EXPECT_DOUBLE_EQ(expected[i][1], result[i][1]);;
    }
}

TEST(Linspace, 2DHalfBorder) {
    std::vector<std::vector<double>> expected = {
        {  1, 3.1}, {  1, 3.2}, {  1, 3.3}, {  1, 3.4},
        {1.1, 3.1}, {1.1, 3.2}, {1.1, 3.3}, {1.1, 3.4},
        {1.2, 3.1}, {1.2, 3.2}, {1.2, 3.3}, {1.2, 3.4}};
    Range<Vec2d> result = linspace(Vec2d{1, 3}, {1.2, 3.5}, {3, 4}, {true, false});
    ASSERT_EQ(expected.size(), result.size());
    for (size_t i = 0; i < expected.size(); ++i) {
        EXPECT_DOUBLE_EQ(expected[i][0], result[i][0]);;
        EXPECT_DOUBLE_EQ(expected[i][1], result[i][1]);;
    }
}

TEST(Linspace, 3DBorder) {
    std::vector<std::vector<double>> expected = {
        {   0,   1, 2}, {   0,   1, 2.5}, {   0,   1, 3},
        {   0, 1.5, 2}, {   0, 1.5, 2.5}, {   0, 1.5, 3},
        {   0,   2, 2}, {   0,   2, 2.5}, {   0,   2, 3},
        {-0.5,   1, 2}, {-0.5,   1, 2.5}, {-0.5,   1, 3},
        {-0.5, 1.5, 2}, {-0.5, 1.5, 2.5}, {-0.5, 1.5, 3},
        {-0.5,   2, 2}, {-0.5,   2, 2.5}, {-0.5,   2, 3},
        {  -1,   1, 2}, {  -1,   1, 2.5}, {  -1,   1, 3},
        {  -1, 1.5, 2}, {  -1, 1.5, 2.5}, {  -1, 1.5, 3},
        {  -1,   2, 2}, {  -1,   2, 2.5}, {  -1,   2, 3}};
    Range<Vec3d> result = linspace(Vec3d{0, 1, 2}, {-1, 2, 3}, {3, 3, 3});
    ASSERT_EQ(expected.size(), result.size());
    for (size_t i = 0; i < expected.size(); ++i) {
        EXPECT_DOUBLE_EQ(expected[i][0], result[i][0]);;
        EXPECT_DOUBLE_EQ(expected[i][1], result[i][1]);;
        EXPECT_DOUBLE_EQ(expected[i][2], result[i][2]);;
    }
}

TEST(Linspace, 3DNoBorder) {
    std::vector<std::vector<double>> expected = {{-0.5, 1.5, 2.5}};
    Range<Vec3d> result = linspace(Vec3d{0, 1, 2}, {-1, 2, 3}, {1, 1, 1}, false);
    ASSERT_EQ(expected.size(), result.size());
    for (size_t i = 0; i < expected.size(); ++i) {
        EXPECT_DOUBLE_EQ(expected[i][0], result[i][0]);;
        EXPECT_DOUBLE_EQ(expected[i][1], result[i][1]);;
        EXPECT_DOUBLE_EQ(expected[i][2], result[i][2]);;
    }
}

TEST(Linspace, 3DHalfBorder) {
    std::vector<std::vector<double>> expected = {
        {-0.5,   1, 2.5}, {-0.5, 1.5, 2.5}, {-0.5,   2, 2.5}};
    Range<Vec3d> result = linspace(Vec3d{0, 1, 2}, {-1, 2, 3}, {1, 3, 1}, {false, true, false});
    ASSERT_EQ(expected.size(), result.size());
    for (size_t i = 0; i < expected.size(); ++i) {
        EXPECT_DOUBLE_EQ(expected[i][0], result[i][0]);;
        EXPECT_DOUBLE_EQ(expected[i][1], result[i][1]);;
        EXPECT_DOUBLE_EQ(expected[i][2], result[i][2]);;
    }
}

TEST(Random, Choice) {
    // random choice -- deterministic tests
    Range<int> elements = {1, 2, 3};
    Range<double> weights = {0, 3, 0};
    EXPECT_EQ(random_choice(elements, weights), 2);
    weights = {0, 1, 0};
    EXPECT_EQ(random_choice(elements, weights, true), 2);

    elements = {0, 1, 2};
    std::mt19937 generator(42);
    std::vector<int> counts = {0, 0, 0};
    for (int i = 0; i < 100; ++i) {
        int c = random_choice(elements, {1, 1, 1}, false, generator);
        EXPECT_LE(0, c);
        EXPECT_LE(c, 2);
        counts[c]++;
    }
    EXPECT_LE(30, counts[0]);
    EXPECT_LE(counts[0], 37);
    EXPECT_LE(30, counts[1]);
    EXPECT_LE(counts[1], 37);
    EXPECT_LE(30, counts[2]);
    EXPECT_LE(counts[2], 37);
}

TEST(Misc, ReshapeVec2DTests) {
    Range<double> valid_input2D = {1.2, 3.2, 2.3, 4.3, 3.2, 3.5};
    Range<double> valid_input3D = {1.2, 3.2, 2.3, 4.3, 3.5, 12, 234.6, 346.6, 12.6};

    Range<double> invalid_input2D = {1.2, 3.2, 2.3, 4.3, 3.2};
    Range<double> invalid_input3D = {1.2, 3.2, 2.3, 4.3, 3.5, 12, 234.6, 346.6};

    Range<Vec2d>  out2D;
    Range<Vec3d>  out3D;

    EXPECT_DEATH(reshape_vec(invalid_input2D, out2D), "");
    EXPECT_DEATH(reshape_vec(invalid_input3D, out3D), "");

    reshape_vec(valid_input2D, out2D);
    reshape_vec(valid_input3D, out3D);

    EXPECT_DOUBLE_EQ(out2D[0][0], valid_input2D[0]);
    EXPECT_DOUBLE_EQ(out2D[1][0], valid_input2D[1]);
    EXPECT_DOUBLE_EQ(out2D[2][0], valid_input2D[2]);
    EXPECT_DOUBLE_EQ(out2D[0][1], valid_input2D[3]);
    EXPECT_DOUBLE_EQ(out2D[1][1], valid_input2D[4]);
    EXPECT_DOUBLE_EQ(out2D[2][1], valid_input2D[5]);

    EXPECT_DOUBLE_EQ(out3D[0][0], valid_input3D[0]);
    EXPECT_DOUBLE_EQ(out3D[1][0], valid_input3D[1]);
    EXPECT_DOUBLE_EQ(out3D[2][0], valid_input3D[2]);
    EXPECT_DOUBLE_EQ(out3D[0][1], valid_input3D[3]);
    EXPECT_DOUBLE_EQ(out3D[1][1], valid_input3D[4]);
    EXPECT_DOUBLE_EQ(out3D[2][1], valid_input3D[5]);
    EXPECT_DOUBLE_EQ(out3D[0][2], valid_input3D[6]);
    EXPECT_DOUBLE_EQ(out3D[1][2], valid_input3D[7]);
    EXPECT_DOUBLE_EQ(out3D[2][2], valid_input3D[8]);
}

}  // namespace mm
