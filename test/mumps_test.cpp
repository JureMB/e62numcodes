#include "gtest/gtest.h"


/**
 * @file test_mls.cpp
 * @brief MLS Tests
 */

#include "mumps.hpp"
#define JOB_INIT -1
#define JOB_END -2
#define USE_COMM_WORLD -987654

namespace mm {

TEST(MUMPS, DISABLED_Example) {
  DMUMPS_STRUC_C id;
  MUMPS_INT n = 2;
  MUMPS_INT8 nnz = 2;
  MUMPS_INT irn[] = {1, 2};
  MUMPS_INT jcn[] = {1, 2};
  double a[2];
  double rhs[2];

  MUMPS_INT myid, ierr;

  int error = 0;
  ierr = MPI_Init(NULL, NULL);
  ierr = MPI_Comm_rank(MPI_COMM_WORLD, &myid);
  /* Define A and rhs */
  rhs[0] = 1.0; rhs[1] = 4.0;
  a[0] = 1.0; a[1] = 2.0;

  /* Initialize a MUMPS instance. Use MPI_COMM_WORLD */
  id.comm_fortran = USE_COMM_WORLD;
  id.par = 1; id.sym = 0;
  id.job = JOB_INIT;
  dmumps_c(&id);

  /* Define the problem on the host */
  if (myid == 0) {
    id.n = n; id.nnz = nnz; id.irn = irn; id.jcn = jcn;
    id.a = a; id.rhs = rhs;
  }
#define ICNTL(I) icntl[(I)-1] /* macro s.t. indices match documentation */
  /* No outputs */
  id.ICNTL(1) = -1; id.ICNTL(2) = -1; id.ICNTL(3) = -1; id.ICNTL(4) = 0;

  /* Call the MUMPS package (analyse, factorization and solve). */
  id.job = 6;
  dmumps_c(&id);
  if (id.infog[0] < 0) {
    printf(" (PROC %d) ERROR RETURN: \tINFOG(1)= %d\n\t\t\t\tINFOG(2)= %d\n",
        myid, id.infog[0], id.infog[1]);
    error = 1;
  }

  /* Terminate instance. */
  id.job = JOB_END;
  dmumps_c(&id);
  if (myid == 0) {
    if (!error) {
      printf("Solution is : (%8.2f  %8.2f)\n", rhs[0], rhs[1]);
    } else {
      printf("An error has occured, please check error code returned by MUMPS.\n");
    }
  }
  ierr = MPI_Finalize();
}

TEST(MUMPS, Constructor) {
    /// Eigen matrix type
    typedef typename Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> ei_matrix_t;
    /// Eigen vector type
    typedef typename Eigen::Matrix<double, Eigen::Dynamic, 1> ei_vec_t;
    prn("start");
    int N = 1000000;
    Eigen::SparseMatrix<double> a(N, N);
    ei_vec_t b(N);
    prn("alloc");
    std::vector< Eigen::Triplet<double> > tripletList;
    tripletList.reserve(3*N-4);
    for (int i = 0; i < N; i++) {
        if (i != 0 && i != N-1) {
            tripletList.push_back({i, i-1, 1});
            tripletList.push_back({i, i, -2});
            tripletList.push_back({i, i+1, 1});
            b(i) = 0;
        } else {
            tripletList.push_back({i, i, 1});
            b(i) = 1;
        }
    }
    a.setFromTriplets(tripletList.begin(), tripletList.end());


    prn("Make solver");
    MumpsSolver<double> solver;
    prn("compute solver");
    solver.compute(a);
    prn("solve solver");
    auto sol = solver.solve(b);
    prn(sol(N/2));
}

}  // namespace mm
